$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    if($('.datetimepicker').length > 0){
        $('.datetimepicker').daterangepicker({
            timePicker: true,
            singleDatePicker:true,
            timePicker24Hour:true,
            timePickerSeconds:true,
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss'
            }
        });
    }

    // Delete Record
    if($('.delete').length > 0){
        $('.delete').click(function(){
            deleteRecord($(this).data('route'));
        });
    }

    // Delay remove Alert
    if($('.alert').length > 0){
        setTimeout(function() {
            $('.alert').fadeOut();
         }, 5000);
    }

    // Set checkbox on Role Set
    if($('.module.form-check').length > 0){
        $('.module.form-check').click(function(){
            if($(this).find('input').is(':checked')){
                $(this).parent().find('.permission').each(function(){
                    $(this).find('span').addClass('checked');
                    $(this).find('input').prop('checked', true);
                })
            }else{
                $(this).parent().find('.permission').each(function(){
                    $(this).find('span').removeClass('checked');
                    $(this).find('input').prop('checked', false);
                })
            }
        })
    }
    if($('.permission').length > 0){
        $('.permission').click(function(){
            var hasOnePermission = false;

            if($(this).find('input').is(':checked')){
                $(this).parent().find('.module').find('input').prop('checked', true);
                $(this).parent().find('.module').find('span').addClass('checked');
            }
            $(this).parent().find('.permission').each(function(){
                if($(this).find('input').is(':checked')){
                    hasOnePermission = true;
                }
            })
            if(!hasOnePermission){
                $(this).parent().find('.module').find('input').prop('checked', false);
                $(this).parent().find('.module').find('span').removeClass('checked');
            }

        });
    }
    // End checkbox on Role Set

    var deleteRecord = function(route){
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((isConfirm) => {
            if(isConfirm){
                $('.overlay').show();
                $.ajax({
                    type:'DELETE',  
                    url:route,
                    dataType: "JSON",
                    data: {},
                    success:function(data){
                        if(data.success){
                            swalOnResult('Deleted Successfully!','success');
                            location.reload();
                        }else{
                            swalOnResult('Error While Deleting!','error');
                        }
                    },
                    error:function(){
                        swalOnResult('Error While Deleting!','error');
                    }
                });      
            }
        });
    }

    var swalOnResult = function(title, type){
        swal({
            title: title,
            text: "You clicked the button!",
            icon: type,
            timer: 1500,
        });
    }
});

// Set Input Currency Format
$(document).ready(function(){
    if($('.input-amount').length > 0){
        $('.input-amount').keyup(function(event){
            var amount = $(this).val() == '' ? '0' : parseInt($(this).val().replace(/[^0-9.]/g, ""));
            var amountFormat = amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            $(this).val(amountFormat);
        });
    }
});

// Check Sidebar Active
$(document).ready(function(){
    var current = (location.origin).concat(location.pathname).concat(location.hash);
    $('.nav-item a').each(function(){
        if ($(this).attr('href') == current) {
            $(this).parents('.nav-item-submenu').addClass('nav-item-expanded nav-item-open');
            $(this).addClass('active');
        }else{
            if(!$(this).parents('.nav-item-submenu').hasClass('nav-item-expanded nav-item-open')){
                $(this).removeClass('active');
            }
        } 
    })
})

// User Sale Network Search
$(document).ready(function(){
    if($('#state').length > 0){
        $('#state').change(function(){ 
            window.location = replaceUrlParam(window.location.href,'state',$('#state').val());
        });
    }
    if($('.appendQueryBtn').length > 0){
        $('.appendQueryBtn').click(function(){
            $(this).parents('form').submit(function(e){
                e.preventDefault(); 
            }); 
            window.location = replaceUrlParam(window.location.href,'keyword',$('#keyword').val());
        })
    }
    function replaceUrlParam(url, paramName, paramValue)
    {
        if (paramValue == null) {
            paramValue = '';
        }
        var pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');
        if (url.search(pattern)>=0) {
            return url.replace(pattern,'$1' + paramValue + '$2');
        }
        url = url.replace(/[?#]$/,'');
        return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;
    }
})