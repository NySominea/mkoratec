var option = {}
option.toolbar = [
            ['redo',['redo']],
            ['undo', ['undo']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['picture',['picture']],
            ['link',['link']],
            ['height', ['height']],
            ['fullscreen', ['fullscreen']],
            ['codeview',['codeview']],
            
];
option.fontNames = ['Khmer OS Battambong', 'Poppins'];
option.fontNamesIgnoreCheck = ['Khmer OS Battambong'];
option.height = 200;

$('.summernote').summernote({
  toolbar: option.toolbar,
  fontNames: option.fontNames,
  fontNamesIgnoreCheck: option.fontNamesIgnoreCheck,
  height: '100%',
  callbacks: {
      onImageUpload: function(files, editor, welEditable) {
          sendFile(files[0], editor, welEditable,$(this));
      }
  }
});


function sendFile(file, editor, welEditable,selector) {
  var saveUrl = selector.data('save-image-url') ? selector.data('save-image-url') : window.location.origin + '/admin/save-summernote-image';
  data = new FormData();
  data.append("file", file);
  $.ajax({
      data: data,
      type: "POST",
      url: saveUrl,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {
          console.log(response.url);
          selector.summernote('editor.insertImage', response.url);
      }
  });
}