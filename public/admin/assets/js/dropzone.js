// Save Single Image Dropzone
if($('#constant').length > 0){
    var constant = JSON.parse($('#constant').val()); 
    var dropzones = [
        { 'selector' : $('#dropzone_'+constant.MAIN_LOGO), 'width' : 370, 'height' : 125},
        {'selector' :$('#dropzone_'+constant.NON_TEXT_LOGO), 'width' : 200, 'height' : 200},
        {'selector' :$('#dropzone_'+constant.TEXT_ONLY_LOGO), 'width' : 370, 'height' : 125},
        {'selector' :$('#dropzone_'+constant.SECONDARY_LOGO), 'width' : 200, 'height' : 200},
    ];
}
$.each(dropzones, function( index, value ) {
    if(value.selector.length > 0){
        var myDropzone = value.selector;
        var key = myDropzone.data('key');
        var width = value.width;
        var height = value.height;
        myDropzone.dropzone({
            acceptedFiles: "image/jpeg, image/png, image/jpg, image/gif",
            maxFiles: 1,
            uploadMultiple: false,
            addRemoveLinks: false,
            dictRemoveFileConfirmation: "Are you sure you want to remove this File?",
            thumbnailWidth: width,
            thumbnailHeight: height,
        
            init: function(){
                var thisDropzone = this;
                if($('#'+key).data('url') != ''){
                    var name = $('#'+key).data('name');
                    var size = $('#'+key).data('size');
                    var url  = $('#'+key).data('url');
                    if(url){
                        myDropzone.css('height','auto');
                        var mockFile = { name: name, size: size, accepted: true };  
                        
                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, url);
                        thisDropzone.files.push(mockFile);
                        thisDropzone.emit("complete", mockFile);
                        thisDropzone.options.maxFiles = thisDropzone.options.maxFiles;
                        thisDropzone._updateMaxFilesReachedClass();
                    }
                }
                this.on('sending', function(file, xhr, formData){
                    formData.append("_token", $("input[name='_token']").val());
                    formData.append("_method", "POST");
                    formData.append('key',key);
                })
                this.on("success", function(file, response){
                    $('#'+key).val(response.path);
                }),
                this.on("addedfile", function(event) {
                    while (thisDropzone.files.length > thisDropzone.options.maxFiles) {
                        thisDropzone.removeFile(thisDropzone.files[0]);
                    }
                    myDropzone.css('height','auto');
                });
            } 
        })
    }
});


if($('#dropzoneProfile').length > 0 || $('#dropzoneBanner').length > 0 ||  
    $('#dropzoneService').length > 0 ||  $('#dropzoneMember').length > 0 ||
    $('#dropzonePartner').length > 0 ||  $('#dropzoneClient').length > 0 ||
    $('#dropzonePortfolio').length > 0 ||  $('#dropzoneBlog').length > 0 
    ){
    if($('#dropzoneProfile').length > 0){
        var myDropzone = $('#dropzoneProfile');
        var width = 200;
        var height = 200;
    }else if($('#dropzoneBanner').length > 0){
        var myDropzone = $('#dropzoneBanner');
        var width = 400;
        var height = 200;
    }
    else if($('#dropzoneService').length > 0){
        var myDropzone = $('#dropzoneService');
        var width = 100;
        var height = 100;
    }
    else if($('#dropzoneMember').length > 0){
        var myDropzone = $('#dropzoneMember');
        var width = 200;
        var height = 200;
    }
    else if($('#dropzonePartner').length > 0){
        var myDropzone = $('#dropzonePartner');
        var width = 200;
        var height = 200;
    }
    else if($('#dropzoneClient').length > 0){
        var myDropzone = $('#dropzoneClient');
        var width = 200;
        var height = 200;
    }
    else if($('#dropzonePortfolio').length > 0){
        var myDropzone = $('#dropzonePortfolio');
        var width = 200;
        var height = 200;
    }
    else if($('#dropzoneBlog').length > 0){
        var myDropzone = $('#dropzoneBlog');
        var width = 300;
        var height = 250;
    }
    myDropzone.dropzone({
        acceptedFiles: "image/jpeg, image/png, image/jpg, image/gif",
        maxFiles: 1,
        uploadMultiple: false,
        addRemoveLinks: true,
        dictRemoveFileConfirmation: "Are you sure you want to remove this File?",
        thumbnailWidth: width,
        thumbnailHeight: height,
    
        init: function(){
            var thisDropzone = this;
            if($('#action').val() == 'update'){
                var name = $('#image').data('name');
                var size = $('#image').data('size');
                var url  = $('#image').data('url');
                if(url){
                    myDropzone.css('height','auto');
                    var mockFile = { name: name, size: size, accepted: true };  
                    
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, url);
                    thisDropzone.files.push(mockFile);
                    thisDropzone.emit("complete", mockFile);
                    thisDropzone.options.maxFiles = thisDropzone.options.maxFiles;
                    thisDropzone._updateMaxFilesReachedClass();
                }
            }
            this.on('sending', function(file, xhr, formData){
                formData.append("_token", $("input[name='_token']").val());
                formData.append("_method", "POST");
            })
            this.on("success", function(file, response){
                $('#image').val(response.path);
            }),
            this.on("addedfile", function(event) {
                while (thisDropzone.files.length > thisDropzone.options.maxFiles) {
                    thisDropzone.removeFile(thisDropzone.files[0]);
                }
                myDropzone.css('height','auto');
            });
            this.on("removedfile", function(file){
                file.previewElement.remove();
                if(this.files.length < 1){
                    myDropzone.css('height','');
                }
            })
        } 
    })
}

// End Save Single Image Dropzone
