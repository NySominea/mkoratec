$(document).ready(() => {
    $('.background-image').each(function() {
        const selector = $(this)
        const smUrl = selector.data('sm');
        const mdUrl = selector.data('md');
        const lgUrl = selector.data('lg');
        const originalUrl = selector.data('original');
        setBackgroundImage(selector, { smUrl, mdUrl, lgUrl, originalUrl })
    })

    function setBackgroundImage(selector, medias) {
        const width = $(window).outerWidth();
        let url = ''

        if (width >= 992) {
            url = medias.lgUrl
        } else if (width >= 576) {
            url = medias.mdUrl
        } else {
            url = medias.smUrl
        }

        if (url === undefined) {
            url = medias.originalUrl
        }
        setTimeout(function() {
            selector.css('background-image', `url(${url})`)
        }, 1000)
    }
})
