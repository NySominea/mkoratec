
// NAVBAR MENU
$(document).ready(function(){
    let windowScroll = $(window).scrollTop()
    let nav = $("#navbar");

    // navbar trasnformation on scroll
    changeNavStyleOnScroll(windowScroll);
    $(window).scroll(function() {
      windowScroll = $(this).scrollTop();
      changeNavStyleOnScroll(windowScroll);
    });

    // navbar collapse on scroll
    $('.collapse').on('shown.bs.collapse', function () {
        if(!nav.hasClass('bg-main')){
            nav.addClass('bg-main shadow');
        }
    });
    $('.collapse').on('hidden.bs.collapse', function () {
        if(nav.hasClass('bg-main') && windowScroll == 0){
            nav.removeClass('bg-main shadow');
        }
    });

    function changeNavStyleOnScroll(){
        if(windowScroll >= nav.height()) {
            nav.addClass('bg-main shadow');
        }else{
            windowScroll = 0;
            nav.removeClass('bg-main shadow')
        }
        $('.navbar-collapse').collapse('hide');
    }

    // Bootstrap Scrollspy
    let collapse = 0;
    $(document).ready(function(){
        $('.collapse').on('shown.bs.collapse', function () {
            collapse = $('.navbar-collapse').height();
        });
    
        $('.collapse').on('hidden.bs.collapse', function () {
            collapse = 0;
        }); 
    })
    $('body').scrollspy({target: ".navbar", offset: nav.outerHeight() + collapse});
    var $root = $('html, body');
    $('#navbar .nav-item a[href^="#"], .learn-more a[href^="#"]').click(function() {
        var href = $.attr(this, 'href');
        let offsetValue  = $(href).offset().top - nav.outerHeight() +collapse;
        $root.animate({
            scrollTop: offsetValue,
        }, 500, function () {
            // window.location.hash = href;
        });

        return false;
    });
    
})


// END NAVBAR MENU

// OWL CAROUSEL
$('#homepage .our-team .owl-carousel').owlCarousel({
    loop:true,
    margin:40,
    nav:true,
    autoplay:true,
    autoplayTimeout:10000,
    autoHeight:true,
    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    responsive:{
        0:{
            items:1,
            nav:false,
        },
        600:{
            items:1,
            nav:true,
            dots:true,
        },
        768:{
            items:1,
            nav:true,
            dots:true,
        },
        1200:{
            items:1,
            nav:true,
            dots:true,
        }
    }
});
$('#homepage .our-partner .owl-carousel').owlCarousel({
    loop:true,
    nav:false,
    dots:false,
    autoplay:true,
    autoplayTimeout: 3000,
    slideTransition: 'linear',
    smartSpeed:3000,
    margin:50,
    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    responsive:{
        0:{
            items:3,
            loop:$('.our-partner .owl-carousel').children().length > 3 ? true : false, 
            margin: $('.our-partner .owl-carousel').children().length > 2 ? 30 : $('.our-partner .owl-carousel').children().length == 1 ? 0 : 15
        },
        420:{
            items:4,
            loop:$('.our-partner .owl-carousel').children().length > 4 ? true : false, 
            margin: $('.our-partner .owl-carousel').children().length > 3 ? 30 : $('.our-partner .owl-carousel').children().length == 1 ? 0 : 15
        },
        991:{
            items:5,
            loop:$('.our-partner .owl-carousel').children().length > 5 ? true : false, 
            margin: $('.our-partner .owl-carousel').children().length > 4 ? 50 : $('.our-partner .owl-carousel').children().length == 1 ? 0 : 30
        },
        1200:{
            items:6,
            loop:$('.our-partner .owl-carousel').children().length > 6 ? true : false, 
            margin: $('.our-partner .owl-carousel').children().length > 5 ? 50 : $('.our-partner .owl-carousel').children().length == 1 ? 0 : 30
        }
    }
});
$('#homepage .our-client .owl-carousel').owlCarousel({
    loop: true,
    margin: 40,
    autoplay: true,
    autoplayTimeout: 3000,
    slideTransition: 'linear',
    smartSpeed:3000,
    autoplayHoverPause: true,
    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    responsive:{
        0:{
            items:3,
            loop:$('.our-client .owl-carousel').children().length > 3 ? true : false, 
            margin: $('.our-client .owl-carousel').children().length > 2 ? 30 : $('.our-client .owl-carousel').children().length == 1 ? 0 : 15
        },
        420:{
            items:4,
            loop:$('.our-client .owl-carousel').children().length > 4 ? true : false, 
            margin: $('.our-client .owl-carousel').children().length > 3 ? 30 : $('.our-client .owl-carousel').children().length == 1 ? 0 : 15
        },
        991:{
            items:5,
            loop:$('.our-client .owl-carousel').children().length > 5 ? true : false, 
            margin: $('.our-client .owl-carousel').children().length > 4 ? 50 : $('.our-client .owl-carousel').children().length == 1 ? 0 : 30
        },
        1200:{
            items:6,
            loop:$('.our-client .owl-carousel').children().length > 6 ? true : false, 
            margin: $('.our-client .owl-carousel').children().length > 5 ? 50 : $('.our-client .owl-carousel').children().length == 1 ? 0 : 30
        }
    }
});

//====Scroll UP button ====
$(document).ready(function(){
    btn = $("#btnScrollUp");

    btn.click(function(){
        $('html,body').animate({
            'scrollTop' : 0,
        },500)
    });
    $(window).on('scroll',function(){
        if($(this).scrollTop() > 100){
            if(!btn.is(':visible')){
                btn.fadeIn('slow');
            }
        }else{
            btn.fadeOut();
        }
    })
});

// RESIZE HEIGHT SLIDER 
resizeSlider();
$(window).resize(function(){
    resizeSlider();
});


function resizeSlider(){
    var height = $(window).outerHeight();
    var width = $(window).outerWidth();
    var selector = $('#homepage .carousel-inner .carousel-item');
    if(width > 468){
        height = height;
    }
    // else if(width > 576){
    //     height = (height * 5) / 6;
    // }else if(width > 468){ 
    //     height = (height * 5) / 7;
    // }
    else{
        height = height / 1.5;
    } 
    selector.css('height',height + 'px');
}


