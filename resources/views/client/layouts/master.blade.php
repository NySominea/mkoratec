<!DOCTYPE html>
<html lang="en">
<head>
    @if(config('app.env') === 'production')
        @include('client.layouts.ga')
    @endif
    @include('client.layouts.head')
</head>

<body>
    @if(config('app.env') === 'production')
        @include('client.layouts.fb')
    @endif
    @include('client/layouts.header')
    <section>
        @yield('content')
    </section>
    @include('client/layouts.footer')
    {{-- <div id="btnScrollUp">
        <button class="btn shadow" style=""><i class="fa fa-arrow-up"></i></button>
    </div> --}}
    <input type="hidden" value="{{url('/')}}" id="baseUrl">
</body>
@include('client.layouts.script')
@yield('custom-js')
</html>
