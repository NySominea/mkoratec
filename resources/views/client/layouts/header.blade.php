<nav id="navbar" class="navbar navbar-expand-lg navbar-dark fixed-top">
    <div class="container">
        @php $image = isset($settings['company'][$constants['MAIN_LOGO']]) ? $settings['company'][$constants['MAIN_LOGO']]->getFirstMedia('images') : null;  @endphp
        <a class="navbar-brand" href="{{url('/')}}"><img src="{{$image ? $image->getUrl() : ''}}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#banner">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#about-us">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#service">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#portfolio">Portfolios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#our-team">Team</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#blog">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact-us">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>