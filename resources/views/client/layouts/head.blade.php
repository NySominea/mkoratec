<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<title>
    @yield('title')
</title>
<link rel="icon" type="image/png" href="{{ asset('client/images/favicon.png') }}">
<link rel="apple-touch-icon" href="{{ asset('client/images/favicon.png') }}">

@php $image = isset($settings['company'][$constants['NON_TEXT_LOGO']]) ? $settings['company'][$constants['NON_TEXT_LOGO']]->getFirstMediaUrl('images') : null;  @endphp
<meta property="og:url"           content="{{ url('/') }}" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="Mkoratec - Infinite Solution" />
<meta property="og:description"   content="" />
<meta property="og:image"         content="{{$image}}" />
<meta property="og:image:width" content="1024">
<meta property="og:image:height" content="1024">

@yield('meta-tag')
<meta property="og:site_name" content="">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link rel="stylesheet" href="{{asset('client/vendor/owlcarousel/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('client/vendor/owlcarousel/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('client/css/custom.css')}}">
