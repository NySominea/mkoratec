@extends('client.layouts.master')

@section('title')
    {{ isset($settings['company'][$constants['SITE_NAME']]) ? $settings['company'][$constants['SITE_NAME']]->value : '' }} - Infinite Solution
@endsection

@section('meta-tag')
<meta name="description" content="{{ isset($settings['company'][$constants['MAIN_SEO_DESCRIPTION']]) ? $settings['company'][$constants['MAIN_SEO_DESCRIPTION']]->value : '' }}">
<meta name="keywords" content="{{ isset($settings['company'][$constants['MAIN_SEO_KEYWORD']]) ? $settings['company'][$constants['MAIN_SEO_KEYWORD']]->value : '' }}">
@endsection

@section('content')
<div id="homepage" class="">

    {{-- Start Banner --}}
    @if($banners->count() > 0)
    <div class="banner" id="banner">
        <div class="banner-slide">
            <div id="slide" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
                <ul class="carousel-indicators">
                    @foreach($banners as $index => $row)
                        <li data-target="#slide" data-slide-to="{{$index}}" class="{{$index == 0 ? 'active' : ''}}"></li>
                    @endforeach
                </ul>
                <div class="carousel-inner">
                    @foreach($banners as $index => $row)
                        @php $image = $row->getFirstMediaUrl('images', 'thumb'); @endphp
                        <div class="background-image carousel-item {{$index == 0 ? 'active' : ''}}"
                            style="background-image: url({{ $image }})"
                            >
                            <div class="overlay">
                                <div class="carousel-caption">
                                    <div class="title my-3">
                                        <h1 class="display-4 text-uppercase font-weight-bold">
                                            {!! $row->title !!}
                                        </h1>
                                    </div>
                                    <div class="description">
                                        {!! $row->description !!}
                                    </div>
                                    <div class="learn-more"><a href="#about-us" class="btn btn-main mt-3">Learn More</a></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif
    {{-- End Banner  --}}

    {{-- Start About Us --}}
    <div id="about-us" class="about-us" style="background-image: url('/client/images/bg.jpg')">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title text-center">
                            <span class="secondary-color">ABOUT</span> <span class="primary-color">US</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <section>
                            <div class="title">Who We Are</div>
                            <div class="description">
                                {!! isset($settings['company'][$constants['COMPANY_DESCRIPTION']]) ? $settings['company'][$constants['COMPANY_DESCRIPTION']]->value : '' !!}
                            </div>
                        </section>
                        <section>
                            <div class="title">Our Core values</div>
                            <div class="description">
                                <section>
                                    <div class="sub-title">
                                        <img width="25px" src="{{asset('client/images/about_us_q.png')}}" alt="">
                                        <span class="head">Integrity</span>
                                    </div>
                                    <p>We respected and valued the “Integrity”. We believed in what we provided to our team work, clients and other people in community will make stronger relationship.</p>
                                </section>
                                <section>
                                    <div class="sub-title">
                                        <img width="25px" src="{{asset('client/images/about_us_kp.png')}}" alt="">
                                        <span class="head">Trust</span>
                                    </div>
                                    <p>You will be more confident to work with <b>MKORATEC</b>. We are young professionals, capable and innovative with abundant of experience that can provide you with the best advice to achieve your desired perfection and stay up-to-date with today's technology.</p>
                                </section>
                                <section>
                                    <div class="sub-title">
                                        <img width="25px" src="{{asset('client/images/about_us_ec.png')}}" alt="">
                                        <span class="head">Quality</span>
                                    </div>
                                    <p>We ensure our customers achieve their goals and quality according to the <b>"Best Price, Best Service, Best Quality"</b>.</p>
                                </section>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-5 text-center my-auto img">
                        @php $image = isset($settings['company'][$constants['NON_TEXT_LOGO']]) ? $settings['company'][$constants['NON_TEXT_LOGO']]->getFirstMedia('images') : null;  @endphp
                        <img src="{{ $image ? $image->getUrl() : ''}}" alt="Mkoratec">
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- End About Us --}}

    {{-- Start Services --}}
    @if($services->count() > 0)
    <div id="service" class="service">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <span class="text-white">ABOUT</span> <span class="primary-color">SERVICES</span>
                    </div>
                </div>
            </div>
            <div class="row py-md-4 py-sm-0">
                @foreach($services as $row)
                @php $image = $row->getFirstMediaUrl('images', 'thumb'); @endphp
                <div class="col-lg-3 col-md-6 item">
                    <div class="text-center img">
                        @if($image)
                            <img src="{{ asset($image) }}" alt="">
                        @endif
                    </div>
                    <div class="title text-center">{!! $row->name !!}</div>
                    <div class="description">
                        {!! $row->description !!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
    {{-- End Services --}}

    {{-- Start Portfolio --}}
    @if($portfolios->count() > 0)
    <div id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <span class="secondary-color">OUR</span> <span class="primary-color">PORTFOLIOS</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-menu mb-3">
            <div class="container">
                <ul class="nav nav-portfolio flex-column flex-sm-row" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="true">All Portfolios</a>
                    </li>
                    @foreach($portfolioCategories as $row)
                        @if($row->portfolios->count() > 0)
                        <li class="nav-item">
                            <a class="nav-link" id="pills-{{$row->id}}-tab" data-toggle="pill" href="#pills-{{$row->id}}" role="tab" aria-controls="pills-{{$row->id}}" aria-selected="false">{{$row->name}}</a>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                    <div class="row item-list">
                        @foreach($portfolios as $row)
                            @php $image = $row->getFirstMediaUrl('images', 'thumb') @endphp
                            <div class="col-lg-3 col-md-4 col-sm-6 col-6 item animated fadeIn">
                                <div class="card">
                                        @if($image)
                                        <img src="{{$image}}" class="card-img-top rounded" alt="{{$row->name}}" loading="lazy" width="100%">
                                        @endif
                                    <div class="overlay">
                                        <a href="{{$row->link}}" class="text-decoration-non" target="_BLINK">
                                            <div class="title">{{$row->name}}</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                @php $portByCat = $portfolios->groupBy('portfolio_category_id'); @endphp
                @foreach($portfolioCategories as $row)
                    @if(isset($portByCat[$row->id]))
                    <div class="tab-pane fade" id="pills-{{$row->id}}" role="tabpanel" aria-labelledby="pills-ux-ui-tab">
                        <div class="row item-list ">
                            @foreach($portByCat[$row->id] as $row)
                            @php $image = $row->getFirstMediaUrl('images', 'thumb') @endphp
                            <div class="col-lg-3 col-md-4 col-sm-6 col-6 item animated zoomIn">
                                <div class="card">
                                    @if($image)
                                    <img src="{{$image}}" class="card-img-top rounded" alt="{{$row->name}}" loading="lazy" width="100%">
                                    @endif
                                    <div class="overlay">
                                        <a href="{{$row->link}}" class="text-decoration-non">
                                            <div class="title">{{$row->name}}</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <div class="btn btn-main">Learn More</div>
                </div>
            </div>
        </div>
    </div>
    @endif
    {{-- End Portfolio --}}

    {{-- Start Our Team --}}
    @if($members->count() > 0)
    <div id="our-team" class="our-team">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <span class="text-white">OUR</span> <span class="primary-color">TEAM</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container d-none d-md-block">
            <div class="owl-carousel owl-theme">
                @foreach($members->chunk(4) as $chunk)
                    <div class="row content">
                        @foreach($chunk->values() as $index => $row)
                            @php $image = $row->getFirstMediaUrl('images', 'thumb'); @endphp
                            @if($index == 0)
                            <div class="col-lg-6 order-md-1">
                                <div class="item left">
                                    <div class="img" style="background-image: url({{ $image }});"></div>
                                    <div class="overlay"></div>
                                    <div class="description">
                                        <div class="title">{!! $row->name !!}</div>
                                        <div class="sub-title">{!! $row->position !!}</div>
                                        <div class="text">{!! $row->message !!}</div>
                                    ​</div>
                                </div>
                            </div>
                            @elseif($index == 2)
                            <div class="col-lg-6 order-md-2 order-lg-3">
                                <div class="item right">
                                    <div class="img" style="background-image: url({{ $image }});"></div>
                                    <div class="overlay"></div>
                                    <div class="description">
                                        <div class="title">{!! $row->name !!}</div>
                                        <div class="sub-title">{!! $row->position !!}</div>
                                        <div class="text">{!! $row->message !!}</div>
                                    ​</div>
                                </div>
                            </div>
                            @elseif($index == 1)
                            <div class="col-lg-6 order-md-3 order-lg-2">
                                <div class="item left">
                                    <div class="img" style="background-image: url({{$image}});"></div>
                                    <div class="overlay"></div>
                                    <div class="description">
                                        <div class="title">{!! $row->name !!}</div>
                                        <div class="sub-title">{!! $row->position !!}</div>
                                        <div class="text">{!! $row->message !!}</div>
                                    ​</div>
                                </div>
                            </div>
                            @elseif($index == 3)
                            <div class="col-lg-6 order-md-4">
                                <div class="item right">
                                    <div class="img" style="background-image: url({{$image}});"></div>
                                    <div class="overlay"></div>
                                    <div class="description">
                                        <div class="title">{!! $row->name !!}</div>
                                        <div class="sub-title">{!! $row->position !!}</div>
                                        <div class="text">{!! $row->message !!}</div>
                                    ​</div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container d-md-none">
            <div class="owl-carousel owl-theme">
                @foreach($members->chunk(2) as $chunk)
                    <div class="row content">
                        @foreach($chunk->values() as $index => $row)
                            @php $image = $row->getFirstMedia('images'); @endphp
                            @if($index == 0)
                                <div class="col-lg-6 order-md-1">
                                    <div class="item left">
                                        <div class="img" style="background-image: url({{$image ? $image->getUrl() : ''}});"></div>
                                        <div class="overlay"></div>
                                        <div class="description">
                                            <div class="title">{!! $row->name !!}</div>
                                            <div class="sub-title">{!! $row->position !!}</div>
                                            <div class="text">{!! $row->message !!}</div>
                                        ​</div>
                                    </div>
                                </div>
                            @elseif($index == 1)
                                <div class="col-lg-6 order-md-2 order-lg-3">
                                    <div class="item right">
                                        <div class="img" style="background-image: url({{$image ? $image->getUrl() : ''}});"></div>
                                        <div class="overlay"></div>
                                        <div class="description">
                                            <div class="title">{!! $row->name !!}</div>
                                            <div class="sub-title">{!! $row->position !!}</div>
                                            <div class="text">{!! $row->message !!}</div>
                                        ​</div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
    {{-- End Our Team --}}

    {{-- Start Our Partner --}}
    @if($partners->count() > 0)
    <div id="our-partner" class="our-partner" style="background-image: url('/client/images/bg.jpg')">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title text-center">
                            <span class="secondary-color">OUR</span> <span class="primary-color">PARTNERS</span>
                        </div>
                    </div>
                </div>
                <div class="row py-md-4 py-3">
                    <div class="col-lg-12">
                        <div class="owl-carousel owl-theme">
                            @foreach($partners as $row)
                                @php $image = $row->getFirstMediaUrl('images', 'thumb'); @endphp
                                <a href="{{ $row->link }}" target="_BLANK"><img src="{{ $image ?: '#' }}" alt="{{ $row->name }}" loading="lazy" width="100%"></a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    {{-- End Our Partner --}}

    {{-- Start Our Client --}}
    @if($clients->count() > 0)
    <div class="our-client" style="background-image: url('/client/images/bg.jpg')">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title text-center">
                            <span class="text-white">OUR</span> <span class="primary-color">CLIENTS</span>
                        </div>
                    </div>
                </div>
                <div class="row py-md-4 py-3">
                    <div class="col-lg-12">
                        <div class="owl-carousel owl-theme">
                            @foreach($clients as $row)
                                @php $image = $row->getFirstMediaUrl('images', 'thumb'); @endphp
                                <a href="{{ $row->link }}" target="_BLANK"><img src="{{$image ?: '#'}}" alt="{{ $row->name }}" loading="lazy" width="100%"></a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    {{-- End Our Client --}}

    {{-- Start Blog --}}
    @if($blogs->count() > 0)
    <div id="blog" class="blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <span class="secondary-color">OUR</span> <span class="primary-color">BLOG</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row py-2">
                @foreach($blogs as $row)
                @php $image = $row->getFirstMedia('images'); @endphp
                <div class="col-lg-6 mb-4">
                    <div class="card flex-row flex-wrap item">
                        <div class="img" style="background-image: url({{$image ? $image->getUrl() : ''}})"></div>
                        <div class="col px-md-4 px-sm-3 px-xs-2 py-2">
                            <div class="card-block">
                                <p class="date">{{ date('F d, Y',strtotime($row->created_at)) }}</p>
                                <h5 class="title">{{ $row->title }}</h5>
                                <p class="description">{!! mb_strimwidth(strip_tags($row->description), 0, 280, "..."); !!}</p>
                                <a href="#" class="link">READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{-- <div class="row">
                <div class="col-12 text-center">
                    <div class="btn btn-main">Learn More</div>
                </div>
            </div> --}}
        </div>
    </div>
    @endif
    {{-- End Blog --}}

    {{-- Start Contact Us --}}
    <div id="contact-us" class="contact-us" style="background-image: url('/client/images/bg.jpg')">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title text-center">
                            <span class="text-white">CONTACT</span> <span class="primary-color">US</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row contact">
                    <div class="item col-lg-3 offset-lg-1 col-md-4">
                        <div class="img"><img src="{{asset('client/images/ic_email.png')}}" alt=""></div>
                        <div class="title">Email</div>
                        <div class="description">{{ isset($settings['company'][$constants['EMAIL']]) ? $settings['company'][$constants['EMAIL']]->value : '' }}</div>
                    </div>
                    <div class="item col-lg-4 col-md-4">
                        <div class="img"><img src="{{asset('client/images/ic_phone.png')}}" alt=""></div>
                        <div class="title">Phone</div>
                        <div class="description">{{ isset($settings['company'][$constants['PHONE']]) ? $settings['company'][$constants['PHONE']]->value : '' }}</div>
                    </div>
                    <div class="item col-lg-3 col-md-4">
                        <div class="img"><img src="{{asset('client/images/ic_address.png')}}" alt=""></div>
                        <div class="title">Address</div>
                        <div class="description">{{ isset($settings['company'][$constants['ADDRESS']]) ? $settings['company'][$constants['ADDRESS']]->value : '' }}</div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="container">
                <div class="row social py-md-4 py-3">
                    <div class="col-lg-12">
                        <div class="title">Follow Us</div>
                        <div class="group">
                            @if(isset($settings['company'][$constants['FACEBOOK_URL']]) && $settings['company'][$constants['FACEBOOK_URL']]->value != '')
                            <a href="{{$settings['company'][$constants['FACEBOOK_URL']]->value }}" class="img" target="_BLANK"><img src="{{asset('client/images/ic_facebook.png')}}" alt="Facebook"></a>
                            @endif

                            @if(isset($settings['company'][$constants['INSTAGRAM_URL']]) && $settings['company'][$constants['INSTAGRAM_URL']]->value != '')
                            <a href="{{ $settings['company'][$constants['INSTAGRAM_URL']]->value }}" class="img" target="_BLANK"><img src="{{asset('client/images/ic_instagram.png')}}" alt="Instagram"></a>
                            @endif

                            @if(isset($settings['company'][$constants['LINKIN_URL']]) && $settings['company'][$constants['LINKIN_URL']]->value != '')
                            <a href="{{ $settings['company'][$constants['LINKIN_URL']]->value}}" class="img" target="_BLANK"><img src="{{asset('client/images/ic_linkin.png')}}" alt="Linkin"></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="container">
                <div class="row footer py-3">
                    <div class="col-lg-12 text-center">
                        <div class="description">{{ isset($settings['company'][$constants['COPY_RIGHT']]) ? $settings['company'][$constants['COPY_RIGHT']]->value : '' }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- End Contct Us --}}
</div>
@endsection
