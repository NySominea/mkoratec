<div class="dropzone needsclick dz-clickable" action="{{ route('admin.settings.saveImage') }}" data-key="{{$key}}" id="dropzone_{{$key}}">
    @csrf
</div> 
@php $image = isset($settings['company'][$key]) ? $settings['company'][$key]->getFirstMedia('images') : null; @endphp
@if($image)
    <input type="hidden" name="{{$key}}" id="{{$key}}" value="" data-model-id="{{$settings['company'][$key]->id}}" data-name="{{$image->file_name}}" data-size="{{$image->size}}" data-url="{{ asset($image->getUrl()) }}">
@else
    <input type="hidden" name="{{$key}}" id="{{$key}}">
@endif