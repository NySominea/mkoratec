@extends('admin.layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Role List</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            @canany(['add-new-user'])
            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.users.roles.create') }}" class="btn btn-link btn-primary btn-sm text-white">
                    <i class="icon-plus2 text-white"></i> Add New
                </a>
            </div>
            @endcanany
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('admin.users.index')}}" class="breadcrumb-item">User</a>
                <span class="breadcrumb-item active">Role</span>
                <span class="breadcrumb-item active">List</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @include('admin.includes.success-msg')
        @include('admin.includes.error-msg')
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($roles) && $roles->count() > 0)
                        @foreach($roles as $key => $row)
                        <tr>
                            <td>{{($roles->perPage() * ($roles->currentPage() - 1)) + $key + 1}}</td>
                            <td>{{ $row->name }}</td>
                            <td class="group-btn-action">
                                @canany(['role-modification'])
                                    <a href="{{ route('admin.users.roles.edit',$row->id) }}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2" data-popup="tooltip" title="" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil7"></i></a>
                                @endcanany
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr><td colspan="3">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($roles) && $roles->count() > 0)
        <div class="card-footer">
            @if($roles->hasMorePages())
                <div class="mb-2">
                    {!! $roles->appends(Request::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$roles->firstItem()}} to {{$roles->lastItem()}}
                of  {{$roles->total()}} entries
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
