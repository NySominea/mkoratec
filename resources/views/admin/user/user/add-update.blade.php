@extends('admin.layouts.master')

@section('custom-css')
<style>
    .dropzone .dz-preview .dz-image img{
        width:100%;
    }
    .dropzone .dz-preview .dz-image{
        width: 200px !important;
        height: 200px !important;
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ isset($user) ? 'Edit' : 'Add'}} User</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{route('admin.users.index')}}" class="btn btn-primary">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('admin.users.index')}}" class="breadcrumb-item">User</a>
                <span class="breadcrumb-item active">{{ isset($user) ? 'Edit' : 'Add'}}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($user))
        {{ Form::model($user,['route' => ['admin.users.update',$user->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'admin.users.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('name')?'text-danger':'' }}">Username</label>
                        <div class="position-relative">
                            {{Form::text("name",null,
                                ["class" => "form-control ".($errors->has('name')?'border-danger':''),"placeholder" => "Enter name"])
                            }}
                            @if($errors->has('name'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('name'))
                            <span class="form-text text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('email')?'text-danger':'' }}">Email</label>
                        <div class="position-relative">
                            {{Form::text("email",null,
                                ["class" => "form-control ".($errors->has('email')?'border-danger':''),"placeholder" => "Enter email"])
                            }}
                            @if($errors->has('email'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('email'))
                            <span class="form-text text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('password')?'text-danger':'' }}">Login Password</label>
                        <div class="position-relative">
                            {{Form::password("password",
                                ["class" => "form-control ".($errors->has('password')?'border-danger':''),"placeholder" => "Enter more than 6 characters",'autocomplete' => 'off'])
                            }}
                            @if($errors->has('password'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('password'))
                            <span class="form-text text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('password_confirmation ')?'text-danger':'' }}">Confirm Login Password</label>
                        <div class="position-relative">
                            {{Form::password("password_confirmation",
                                ["class" => "form-control ".($errors->has('password_confirmation')?'border-danger':''),"placeholder" => "Enter more than 6 characters", 'autocomplete' => 'off'])
                            }}
                            @if($errors->has('password_confirmation'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('password_confirmation'))
                            <span class="form-text text-danger">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>

                    <div class="form-group select2">
                        <label>Role Authority</label>
                        {{Form::select("role_id",$roles, isset($user,$user->roles[0]) ? $user->roles[0]->id : null,["class" => "form-control"])}}
                    </div>

                    <div class="form-group mb-3 mb-md-2">
                        <label class="d-block font-weight-semibold">State</label>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="1" class="form-check-input-styled-success" data-fouc checked>
                                Enable
                            </label>
                        </div>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="0" class="form-check-input-styled-danger" data-fouc 
                                    {{isset($user) && !$user->state ? 'checked' : '' }}>
                                    Disable
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('image')?'text-danger':'' }}">Profile Image</label>
                        <div class="dropzone needsclick dz-clickable" action="{{ route('admin.users.saveImage') }}" id="dropzoneProfile">
                            @csrf
                        </div>
                        @if($errors->has('image'))
                            <span class="form-text text-danger">{{ $errors->first('image') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> Save
            </button>
        </div>

        @php $image = isset($user) ? $user->getMedia('images')->first() : null; @endphp
        @if(isset($user) && $image)
            <input type="hidden" name="image" id="image" value="{{old('image')}}" data-model-id="{{$user->id}}" data-name="{{$image->file_name}}" data-size="{{$image->size}}" data-url="{{ asset($image->getUrl()) }}">
        @else
            <input type="hidden" name="image" id="image" value="{{old('image')}}">
        @endif
        <input type="hidden" name="action" id="action" value="{{isset($user) ? 'update' : 'add'}}">
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="{{asset('admin/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>}
<script src="{{asset('admin/global_assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
@endsection