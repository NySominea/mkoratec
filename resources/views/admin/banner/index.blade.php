@extends('admin.layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Banner List</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            @canany(['add-new-user'])
            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.banners.create') }}" class="btn btn-link btn-primary btn-sm text-white">
                    <i class="icon-plus2 text-white"></i> Add New
                </a>
            </div>
            @endcanany
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Banner</span>
                <span class="breadcrumb-item active">List</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @include('admin.includes.success-msg')
        @include('admin.includes.error-msg')
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>Banner</th>
                        <th>Title</th>
                        <th>State</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($banners) && $banners->count() > 0)
                        @foreach($banners as $key => $row)
                        @php $image = $row->getFirstMediaUrl('images', 'thumb'); @endphp
                        <tr>
                            <td>{{($banners->perPage() * ($banners->currentPage() - 1)) + $key + 1}}</td>
                            <td>
                                @if($image)
                                    <img width="100px" src="{{asset($image)}}" alt="{{ $row->name }}">
                                @endif
                            </td>
                            <td>{!! $row->title !!}</td>
                            <td>
                                @if($row->state)
                                    <span class="badge badge-success">Active</span>
                                @else
                                    <span class="badge badge-danger">Deactive</span>
                                @endif
                            </td>
                            <td class="group-btn-action">
                                @canany(['user-modification'])
                                    <a href="{{ route('admin.banners.edit',$row->id) }}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2" data-popup="tooltip" title="" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil7"></i></a>
                                    <button class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 delete" data-route="{{route('admin.banners.destroy',$row->id)}}" data-popup="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="icon-trash"></i></button>
                                @endcanany
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr><td colspan="5">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($banners))
        <div class="card-footer">
            @if($banners->hasMorePages())
                <div class="mb-2">
                    {!! $banners->appends(Request::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$banners->firstItem()}} to {{$banners->lastItem()}}
                of  {{$banners->total()}} entries
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
