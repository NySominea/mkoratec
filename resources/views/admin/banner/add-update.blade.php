@extends('admin.layouts.master')

@section('custom-css')
<style>
    .dropzone .dz-preview .dz-image img{
        width:100%;
    }
    .dropzone .dz-preview .dz-image{
        width: 300px !important;
        height: 225px !important;
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ isset($banner) ? 'Edit' : 'Add'}} Banner</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.banners.create') }}" class="btn btn-primary mr-2">
                    <i class="icon-plus2 text-white"></i> Add New
                </a>
                <a href="{{route('admin.banners.index')}}" class="btn btn-primary">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('admin.banners.index')}}" class="breadcrumb-item">Banner</a>
                <span class="breadcrumb-item active">{{ isset($banner) ? 'Edit' : 'Add'}}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($banner))
        {{ Form::model($banner,['route' => ['admin.banners.update',$banner->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'admin.banners.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('image')?'text-danger':'' }}">Banner Image</label>
                        <div class="dropzone needsclick dz-clickable" action="{{ route('admin.banners.saveImage') }}" id="dropzoneBanner">
                            @csrf
                        </div>
                        @if($errors->has('image'))
                            <span class="form-text text-danger">{{ $errors->first('image') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Title</label>
                        <div class="position-relative">
                            {{Form::text("title",null,
                                ["class" => "form-control ","placeholder" => "Enter Title"])
                            }}
                        </div>
                    </div>

                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Description</label>
                        <div class="position-relative">
                            {{Form::textarea("description",null,
                                ["class" => "form-control ","placeholder" => "Enter Description",'rows' => 5])
                            }}
                        </div>
                    </div>

                    <div class="form-group mb-3 mb-md-2">
                        <label class="d-block font-weight-semibold">State</label>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="1" class="form-check-input-styled-success" data-fouc checked>
                                Enable
                            </label>
                        </div>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="0" class="form-check-input-styled-danger" data-fouc
                                    {{isset($banner) && !$banner->state ? 'checked' : '' }}>
                                    Disable
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> Save
            </button>
        </div>

        @php $image = isset($banner) ? $banner->getMedia('images')->first() : null; @endphp
        @if(isset($banner) && $image)
            <input type="hidden" name="image" id="image" value="{{old('image')}}" data-model-id="{{$banner->id}}" data-name="{{$image->file_name}}" data-size="{{$image->size}}" data-url="{{ asset($image->getUrl('thumb')) }}">
        @else
            <input type="hidden" name="image" id="image" value="{{old('image')}}">
        @endif
        <input type="hidden" name="action" id="action" value="{{isset($banner) ? 'update' : 'add'}}">
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="{{asset('admin/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>}
<script src="{{asset('admin/global_assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
@endsection
