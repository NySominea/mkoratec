@extends('admin.layouts.master')

@section('custom-css')
<style>
    #dropzone_{{ $constants['MAIN_LOGO'] }}.dropzone .dz-preview .dz-image,
    #dropzone_{{ $constants['TEXT_ONLY_LOGO'] }}.dropzone .dz-preview .dz-image{
        width: 300px !important;
        height: 125px !important;
    }
    #dropzone_{{ $constants['NON_TEXT_LOGO'] }}.dropzone .dz-preview .dz-image,
    #dropzone_{{ $constants['SECONDARY_LOGO'] }}.dropzone .dz-preview .dz-image{
        width: 200px !important;
        height: 200px !important;
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Company Setting</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <button form="form" class="btn btn-success">
                    <i class="icon-folder mr-1"></i> Save
                </button>
            </div>
        </div>
    </div>


    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Company Setting</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        {{ Form::open(['route' => 'admin.settings.store', 'method' => 'POST', 'id' => 'form']) }}
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#main" data-target="#main">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#logo">Logo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#contact">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#seo">SEO</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="nav-main-tab">
                            <div class="col-lg-6">
                                <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Company Name</label>
                                    <div class="position-relative">
                                        {{Form::text($constants['SITE_NAME'],isset($settings['company'][$constants['SITE_NAME']]) ? $settings['company'][$constants['SITE_NAME']]->value : null,
                                            ["class" => "form-control ","placeholder" => "Enter Site name"])
                                        }}
                                    </div>
                                </div>
            
                                <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Site URL</label>
                                    <div class="position-relative">
                                        {{Form::text($constants['SITE_URL'],isset($settings['company'][$constants['SITE_URL']]) ? $settings['company'][$constants['SITE_URL']]->value : null,
                                            ["class" => "form-control ","placeholder" => "Enter Site name"])
                                        }}
                                    </div>
                                </div>

                                <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Company Description</label>
                                    <div class="position-relative">
                                        {{Form::textarea($constants['COMPANY_DESCRIPTION'],isset($settings['company'][$constants['COMPANY_DESCRIPTION']]) ? $settings['company'][$constants['COMPANY_DESCRIPTION']]->value : null,
                                            ["class" => "form-control ","placeholder" => "Enter Company description", 'rows' => 4])
                                        }}
                                    </div>
                                </div>

                                <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Copy Right</label>
                                    <div class="position-relative">
                                        {{Form::text($constants['COPY_RIGHT'],isset($settings['company'][$constants['COPY_RIGHT']]) ? $settings['company'][$constants['COPY_RIGHT']]->value : null,
                                            ["class" => "form-control ","placeholder" => "Enter copy right"])
                                        }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="logo" role="tabpanel" aria-labelledby="nav-logo-tab">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Main Logo</label>
                                        @include('admin.includes.single-dropzone',['key' => $constants['MAIN_LOGO']])
                                    </div>
                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Secondary Logo</label>
                                        @include('admin.includes.single-dropzone',['key' => $constants['SECONDARY_LOGO']])
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Non Text Logo</label>
                                        @include('admin.includes.single-dropzone',['key' => $constants['NON_TEXT_LOGO']])
                                    </div>
                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Text Only Logo</label>
                                        @include('admin.includes.single-dropzone',['key' => $constants['TEXT_ONLY_LOGO']])
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Address</label>
                                        <div class="position-relative">
                                            {{Form::text($constants['ADDRESS'],isset($settings['company'][$constants['ADDRESS']]) ? $settings['company'][$constants['ADDRESS']]->value : null,
                                                ["class" => "form-control ","placeholder" => "Enter address"])
                                            }}
                                        </div>
                                    </div>
                
                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Phone number</label>
                                        <div class="position-relative">
                                            {{Form::text($constants['PHONE'],isset($settings['company'][$constants['PHONE']]) ? $settings['company'][$constants['PHONE']]->value : null,
                                                ["class" => "form-control ","placeholder" => "Enter phone"])
                                            }}
                                        </div>
                                    </div>

                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Email</label>
                                        <div class="position-relative">
                                            {{Form::text($constants['EMAIL'],isset($settings['company'][$constants['EMAIL']]) ? $settings['company'][$constants['EMAIL']]->value : null,
                                                ["class" => "form-control ","placeholder" => "Enter Company description"])
                                            }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Facebook URL</label>
                                        <div class="position-relative">
                                            {{Form::text($constants['FACEBOOK_URL'],isset($settings['company'][$constants['FACEBOOK_URL']]) ? $settings['company'][$constants['FACEBOOK_URL']]->value : null,
                                                ["class" => "form-control ","placeholder" => "Enter Facebook URL"])
                                            }}
                                        </div>
                                    </div>
                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Instagram URL</label>
                                        <div class="position-relative">
                                            {{Form::text($constants['INSTAGRAM_URL'],isset($settings['company'][$constants['INSTAGRAM_URL']]) ? $settings['company'][$constants['INSTAGRAM_URL']]->value : null,
                                                ["class" => "form-control ","placeholder" => "Enter Instagram URL"])
                                            }}
                                        </div>
                                    </div>
                                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                        <label class="form-group-float-label font-weight-semibold is-visible">Linkin URL</label>
                                        <div class="position-relative">
                                            {{Form::text($constants['LINKIN_URL'],isset($settings['company'][$constants['LINKIN_URL']]) ? $settings['company'][$constants['LINKIN_URL']]->value : null,
                                                ["class" => "form-control ","placeholder" => "Enter Linkin URL"])
                                            }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="nav-seo-tab">
                            <div class="col-lg-6">
                                <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Main SEO Keywords</label>
                                    <div class="position-relative">
                                        {{Form::text($constants['MAIN_SEO_KEYWORD'],isset($settings['company'][$constants['MAIN_SEO_KEYWORD']]) ? $settings['company'][$constants['MAIN_SEO_KEYWORD']]->value : null,
                                            ["class" => "form-control ","placeholder" => "Enter main SEO keywords"])
                                        }}
                                    </div>
                                </div>
            
                                <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Main SEO Description</label>
                                    <div class="position-relative">
                                        {{Form::textarea($constants['MAIN_SEO_DESCRIPTION'],isset($settings['company'][$constants['MAIN_SEO_DESCRIPTION']]) ? $settings['company'][$constants['MAIN_SEO_DESCRIPTION']]->value : null,
                                            ["class" => "form-control ","placeholder" => "Enter main SEO description", 'rows' => 5])
                                        }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function(){
            $('.nav-tabs .nav-item:first-child .nav-link').addClass('active')
        })
    </script>
@endsection