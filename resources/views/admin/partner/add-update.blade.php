@extends('admin.layouts.master')

@section('custom-css')
<style>
    .dropzone .dz-preview .dz-image{
        width: 200px !important;
        height: 200px !important;
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ isset($partner) ? 'Edit' : 'Add'}} Partner</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.partners.create') }}" class="btn btn-primary mr-2">
                    <i class="icon-plus2 text-white"></i> Add New
                </a>
                <a href="{{route('admin.partners.index')}}" class="btn btn-primary">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('admin.partners.index')}}" class="breadcrumb-item">Partner</a>
                <span class="breadcrumb-item active">{{ isset($partner) ? 'Edit' : 'Add'}}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($partner))
        {{ Form::model($partner,['route' => ['admin.partners.update',$partner->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'admin.partners.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('image')?'text-danger':'' }}">Partner Image (Aspect Ratio 1:1)</label>
                        <div class="dropzone needsclick dz-clickable" action="{{ route('admin.partners.saveImage') }}" id="dropzonePartner">
                            @csrf
                        </div>
                        @if($errors->has('image'))
                            <span class="form-text text-danger">{{ $errors->first('image') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Name</label>
                        <div class="position-relative">
                            {{Form::text("name",null,
                                ["class" => "form-control ","placeholder" => "Enter Name"])
                            }}
                        </div>
                    </div>

                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Link</label>
                        <div class="position-relative">
                            {{Form::text("link",null,
                                ["class" => "form-control ","placeholder" => "Enter Link"])
                            }}
                        </div>
                    </div>

                    <div class="form-group mb-3 mb-md-2">
                        <label class="d-block font-weight-semibold">State</label>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="1" class="form-check-input-styled-success" data-fouc checked>
                                Enable
                            </label>
                        </div>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="0" class="form-check-input-styled-danger" data-fouc 
                                    {{isset($partner) && !$partner->state ? 'checked' : '' }}>
                                    Disable
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> Save
            </button>
        </div>

        @php $image = isset($partner) ? $partner->getMedia('images')->first() : null; @endphp
        @if(isset($partner) && $image)
            <input type="hidden" name="image" id="image" value="{{old('image')}}" data-model-id="{{$partner->id}}" data-name="{{$image->file_name}}" data-size="{{$image->size}}" data-url="{{ asset($image->getUrl()) }}">
        @else
            <input type="hidden" name="image" id="image" value="{{old('image')}}">
        @endif
        <input type="hidden" name="action" id="action" value="{{isset($partner) ? 'update' : 'add'}}">
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="{{asset('admin/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>}
<script src="{{asset('admin/global_assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
@endsection