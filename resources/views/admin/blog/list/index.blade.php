@extends('admin.layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Blog List</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            @canany(['add-new-user'])
            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.blogs.create') }}" class="btn btn-link btn-primary btn-sm text-white">
                    <i class="icon-plus2 text-white"></i> Add New
                </a>
            </div>
            @endcanany
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Blog</span>
                <span class="breadcrumb-item active">List</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @include('admin.includes.success-msg')
        @include('admin.includes.error-msg')
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>Thumbnail</th>
                        <th>Title</th>
                        <th style="width:400px; min-width:200px;">Description</th>
                        <th>Category</th>
                        <th>State</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($blogs) && $blogs->count() > 0)
                        @foreach($blogs as $key => $row)
                        @php $image = $row->getFirstMedia('images'); @endphp
                        <tr>
                            <td>{{($blogs->perPage() * ($blogs->currentPage() - 1)) + $key + 1}}</td>
                            <td>
                                @if($image)
                                    <img width="100px" src="{{asset($image->getUrl())}}" alt="{{ $row->name }}">
                                @endif
                            </td>
                            <td>{!! $row->title !!}</td>
                            <td>{!! mb_strimwidth(strip_tags($row->description), 0, 280, "..."); !!}</td>
                            <td>{!! $row->category ? $row->category->name : '' !!}</td>
                            <td>
                                @if($row->state)
                                    <span class="badge badge-success">Active</span>
                                @else
                                    <span class="badge badge-danger">Deactive</span>
                                @endif
                            </td>
                            <td class="group-btn-action">
                                @canany(['user-modification'])
                                    <a href="{{ route('admin.blogs.edit',$row->id) }}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2" data-popup="tooltip" title="" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil7"></i></a>
                                    <button class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 delete" data-route="{{route('admin.blogs.destroy',$row->id)}}" data-popup="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="icon-trash"></i></button>
                                @endcanany
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr><td colspan="7">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($blogs) && $blogs->count() > 0)
        <div class="card-footer">
            @if($blogs->hasMorePages())
                <div class="mb-2">
                    {!! $blogs->appends(Request::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$blogs->firstItem()}} to {{$blogs->lastItem()}}
                of  {{$blogs->total()}} entries
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
