<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	@include('admin.layouts.head')
</head>
<body>
	@include('admin.layouts.header')  
	
	<!-- Page content -->
	<div class="page-content">
		<!-- Main sidebar -->
		<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navigation
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user">
					<div class="card-body">
						<div class="media">
							@php $auth = auth()->user(); $image = $auth->getFirstMedia('images');@endphp
							<div class="mr-3">
								<a href="#"><img src="{{ $image ? $image->getUrl() : ''}}" width="38" height="38" class="rounded-circle" alt=""></a>
							</div>

							<div class="media-body">
								<div class="media-title font-weight-semibold">{{ $auth ? $auth->name : env('APP_NAME') }}</div>
								<div class="font-size-xs opacity-50">
									{{ $auth &&  $auth->roles && count($auth->roles) > 0 ? $auth->roles[0]->name : '' }}
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /user menu -->


				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">
						@include('admin.layouts.sidebar')
					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->
			
		</div>
		<!-- /main sidebar -->

		<!-- Main content -->
		<div class="content-wrapper">
			@yield('content')
			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; {{ date('Y') }}. <a href="{{ isset($settings['company']['site_url']) ? $settings['company']['site_url']->value : '#' }}">Mkoratec - Infinite Solution</a>
					</span>

				</div>
			</div>
			<!-- /footer -->
		</div>
		<!-- /main content -->
	</div>
	<!-- /page content -->
	<input type="hidden" name="constant" id="constant" value="{{ json_encode(config()->get('constant.SETTING')) }}">
	<input type="hidden" name="baseUrl" id="baseUrl" value="{{ url('/') }}">
	@include('admin.layouts.script')
</body>

</html>
