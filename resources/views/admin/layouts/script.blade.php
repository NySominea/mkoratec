<script src="{{asset('admin/assets/js/app.js')}}"></script>
<script src="{{asset('admin/global_assets/js/main/jquery.min.js')}}"></script>
<script src="{{asset('admin/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('admin/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
<script src="/admin/global_assets/js/plugins/uploaders/dropzone.min.js"></script>
<script src="/admin/global_assets/js/demo_pages/uploader_dropzone.js"></script>
@yield('page-script')
<script src="{{asset('admin/assets/js/custom.js')}}"></script>
<script src="{{asset('admin/assets/js/dropzone.js')}}"></script>
<script src="{{asset('admin/assets/js/summernote.js')}}"></script>

@yield('custom-js')