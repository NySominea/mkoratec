<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>

<li class="nav-item">
    <a href="{{ route('admin.dashboard') }}" class="nav-link"><i class="icon-home4"></i> <span>Dashboard</span></a>
</li>

<li class="nav-item">
    <a href="{{ route('admin.settings.index') }}" class="nav-link"><i class="icon-home4"></i> <span>Company Setting</span></a>
</li>

<li class="nav-item">
    <a href="{{ route('admin.banners.index') }}" class="nav-link"><i class="icon-home4"></i> <span>Banner</span></a>
</li>

<li class="nav-item nav-item-submenu">
    <a href="#" class="nav-link"><i class="icon-coins"></i> <span>Portfolio</span></a>
    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        <li class="nav-item">
            <a href="{{ route('admin.portfolios.index') }}" class="nav-link">List</a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.portfolios.categories.index') }}" class="nav-link">Category</a>
        </li>
    </ul>
</li>

<li class="nav-item">
    <a href="{{ route('admin.services.index') }}" class="nav-link"><i class="icon-home4"></i> <span>Service</span></a>
</li>

<li class="nav-item">
    <a href="{{ route('admin.members.index') }}" class="nav-link"><i class="icon-home4"></i> <span>Team Member</span></a>
</li>

<li class="nav-item nav-item-submenu">
    <a href="#" class="nav-link"><i class="icon-coins"></i> <span>Blog</span></a>
    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        <li class="nav-item">
            <a href="{{ route('admin.blogs.index') }}" class="nav-link">List</a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.blogs.categories.index') }}" class="nav-link">Category</a>
        </li>
    </ul>
</li>

<li class="nav-item">
    <a href="{{ route('admin.partners.index') }}" class="nav-link"><i class="icon-home4"></i> <span>Partner</span></a>
</li>

<li class="nav-item">
    <a href="{{ route('admin.clients.index') }}" class="nav-link"><i class="icon-home4"></i> <span>Client</span></a>
</li>

<li class="nav-item nav-item-submenu">
    <a href="#" class="nav-link"><i class="icon-coins"></i> <span>User</span></a>
    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        <li class="nav-item">
            <a href="{{route('admin.users.index')}}" class="nav-link">User List</a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin.users.roles.index')}}" class="nav-link">Role List</a>
        </li>
    </ul>
</li>

