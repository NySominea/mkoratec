@extends('admin.layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Add Portfolio Category</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.portfolios.categories.create') }}" class="btn btn-primary mr-2">
                    <i class="icon-plus2 text-white"></i> Add New
                </a>
                <a href="{{ route('admin.portfolios.categories.index') }}" class="btn btn-primary">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('admin.portfolios.index')}}" class="breadcrumb-item">Portfolio</a>
                <a href="{{route('admin.portfolios.categories.index')}}" class="breadcrumb-item">category</a>
                <span class="breadcrumb-item active">{{ isset($category) ? 'Edit' : 'Add'}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($category))
        {{ Form::model($category,['route' => ['admin.portfolios.categories.update',$category->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'admin.portfolios.categories.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="form-group row form-group-float form-group-feedback form-group-feedback-right">
                <div class="col-lg-6">
                    <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('name')?'text-danger':'' }}">Category Name</label>
                    <div class="position-relative">
                        {{Form::text("name",null,
                            ["class" => "form-control ".($errors->has('name')?'border-danger':''),"placeholder" => "Enter category name"])
                        }}
                        @if($errors->has('name'))
                            <div class="form-control-feedback text-danger">
                                <i class="icon-spam"></i>
                            </div>
                        @endif
                    </div>
                    @if($errors->has('name'))
                        <span class="form-text text-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> Save
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="{{asset('admin/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>}
<script src="{{asset('admin/global_assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
@endsection