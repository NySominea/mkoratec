@extends('admin.layouts.master')

@section('custom-css')
<style>
    .dropzone .dz-preview .dz-image{
        width: 200px !important;
        height: 200px !important;
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ isset($portfolio) ? 'Edit' : 'Add'}} portfolio</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('admin.portfolios.create') }}" class="btn btn-primary mr-2">
                    <i class="icon-plus2 text-white"></i> Add New
                </a>
                <a href="{{route('admin.portfolios.index')}}" class="btn btn-primary">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('admin.portfolios.index')}}" class="breadcrumb-item">Portfolio</a>
                <span class="breadcrumb-item active">{{ isset($portfolio) ? 'Edit' : 'Add'}}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($portfolio))
        {{ Form::model($portfolio,['route' => ['admin.portfolios.update',$portfolio->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'admin.portfolios.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('name')?'text-danger':'' }}">Name</label>
                        <div class="position-relative">
                            {{Form::text("name",null,
                                ["class" => "form-control ".($errors->has('name')?'border-danger':''),"placeholder" => "Enter username"])
                            }}
                            @if($errors->has('name'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('name'))
                            <span class="form-text text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>

                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('link')?'text-danger':'' }}">Url Link</label>
                        <div class="position-relative">
                            {{Form::text("link",null,
                                ["class" => "form-control ".($errors->has('link')?'border-danger':''),"placeholder" => "Enter url link"])
                            }}
                            @if($errors->has('link'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('link'))
                            <span class="form-text text-danger">{{ $errors->first('link') }}</span>
                        @endif
                    </div>

                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('description')?'text-danger':'' }}">Description</label>
                        <div class="position-relative">
                            {{Form::textarea("description",null,
                                ["class" => "form-control ".($errors->has('description')?'border-danger':''),"placeholder" => "Enter description",'rows' => 5])
                            }}
                            @if($errors->has('description'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('description'))
                            <span class="form-text text-danger">{{ $errors->first('description') }}</span>
                        @endif
                    </div>

                    <div class="form-group select2">
                        <label>Categories</label>
                        {{Form::select("portfolio_category_id",$categories, isset($portfolio,$portfolio->category) ? $portfolio->category->id : null,["class" => "form-control"])}}
                    </div>

                    <div class="form-group mb-3 mb-md-2">
                        <label class="d-block font-weight-semibold">State</label>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="1" class="form-check-input-styled-success" data-fouc checked>
                                Enable
                            </label>
                        </div>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="0" class="form-check-input-styled-danger" data-fouc
                                    {{isset($user) && !$user->state ? 'checked' : '' }}>
                                    Disable
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('image')?'text-danger':'' }}">Image (Aspect Ratio 1:1)</label>
                        <div class="dropzone needsclick dz-clickable" action="{{ route('admin.portfolios.saveImage') }}" id="dropzonePortfolio">
                            @csrf
                        </div>
                        @if($errors->has('image'))
                            <span class="form-text text-danger">{{ $errors->first('image') }}</span>
                        @endif
                    </div>
                </div>

            </div>

        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> Save
            </button>
        </div>

        @php $image = isset($portfolio) ? $portfolio->getMedia('images')->first() : null; @endphp
        @if(isset($portfolio) && $image)
            <input type="hidden" name="image" id="image" value="{{old('image')}}" data-model-id="{{$portfolio->id}}" data-name="{{$image->file_name}}" data-size="{{$image->size}}" data-url="{{ asset($image->getUrl('thumb')) }}">
        @else
            <input type="hidden" name="image" id="image" value="{{old('image')}}">
        @endif
        <input type="hidden" name="action" id="action" value="{{isset($portfolio) ? 'update' : 'add'}}">
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="{{asset('admin/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>}
<script src="{{asset('admin/global_assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
@endsection
