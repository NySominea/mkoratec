<?php

return [
    'SETTING' => [
        'SITE_NAME'     => 'site_name',
        'SITE_URL'        => 'site_url',
        'COMPANY_DESCRIPTION'   => 'company_description',
        'COPY_RIGHT'    => 'copy_right',
        'MAIN_LOGO'     => 'main_logo',
        'SECONDARY_LOGO'=> 'secondary_logo',
        'NON_TEXT_LOGO' => 'non_text_logo',
        'TEXT_ONLY_LOGO'     => 'text_only_logo',
        'MAIN_SEO_KEYWORD'   => 'seo_keyword',
        'MAIN_SEO_DESCRIPTION'=> 'seo_description',
        'FACEBOOK_URL'      => 'facebook_url',
        'INSTAGRAM_URL'       => 'instagram_url',
        'LINKIN_URL'        => 'linkin_url',
        'ADDRESS'        => 'address',
        'EMAIL'         => 'email',
        'PHONE'         => 'phone',
    ]
];