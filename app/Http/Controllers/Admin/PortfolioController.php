<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Portfolio;
use App\Model\PortfolioCategory;

class PortfolioController extends Controller
{
    public function index()
    {
        $portfolios = Portfolio::orderBy('id','DESC')->paginate(10);

        return view('admin.portfolio.list.index',compact('portfolios'));
    }

    public function create()
    {
        $categories = PortfolioCategory::orderBy('name','ASC')->pluck('name','id')->toArray();
        $categories = ['0' => 'Choose a category'] + $categories;

        return view('admin.portfolio.list.add-update', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'required',
            'name' => 'required',
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.portfolios.index')->withSuccess('You have just added a portfolio successfully!');
    }

    public function edit($id)
    {
        $portfolio = Portfolio::findOrFail($id);
        $categories = PortfolioCategory::orderBy('name','ASC')->pluck('name','id')->toArray();
        $categories = ['0' => 'Choose a category'] + $categories;
        return view('admin.portfolio.list.add-update',compact('portfolio','categories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required'
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.portfolios.index')->withSuccess('You have just updated a portfolio successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $portfolio = Portfolio::find($id);
       if($portfolio){
           DB::beginTransaction();
           try{
               if($portfolio->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveImage(Request $request){
       if($request->hasFile('file')){
           $path = saveTempImage($request->file('file'));
           return response()->json(['Status' => true, 'path' => $path]);
       }
       return response()->json(['Status' => false]);
   }

    public function deleteImage(){
        $result = false;
        $portfolio_id = request()->get('portfolio_id');
        DB::beginTransaction();
        try{
            $portfolio = Portfolio::findOrFail($portfolio_id);
            if($portfolio){
                $portfolio->clearMediaCollection('images');
                $result = true;
            }
            DB::commit();
        }catch(Exception $exception){
            DB::rollback();
            $result = false;
        }

        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $portfolio = isset($id) ? Portfolio::find($id) : new portfolio;
            if(!$portfolio) return redirect()->back()->withError('There is no record found!');

            $portfolio->fill($data);
            if($portfolio->save()){

               if(isset($data['image']) && !empty($data['image'])){
                   $filename = generateFileName($portfolio->name);
                   $portfolio->clearMediaCollection('images');
                   $portfolio->addMedia($data['image'])
                        ->usingName($filename)
                        ->usingFileName($filename.'.'.pathinfo($data['image'], PATHINFO_EXTENSION))
                        ->toMediaCollection('images');
               }
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $portfolio;
    }
}
