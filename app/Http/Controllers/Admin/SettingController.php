<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Setting;
use Config;
use DB;

class SettingController extends Controller
{
     public function index()
     {   
         $constants = config()->get('constant.SETTING');
         return view('admin.setting.index',compact('constants'));
     }
 
     public function store(Request $request)
     {   
         $data = $request->all();
         $constants = Config::get('constant.SETTING');
         $imageKey = [
             $constants['MAIN_LOGO'],
             $constants['NON_TEXT_LOGO'],
             $constants['TEXT_ONLY_LOGO'],
             $constants['SECONDARY_LOGO']
         ];
         
         foreach($constants as $index => $key){
            $r = [
                'key' => $key,
                'value' => isset($data[$key]) ? $data[$key] : '',
            ];
             
             $row = Setting::where(['key' => $key])->first();
             if($row){
                 if(in_array($key,$imageKey)){
                     if(isset($data[$key]) && $data[$key]){
                         $row->update($r);
                         $row->clearMediaCollection('images');
                         $row->addMedia($data[$key])
                                 ->toMediaCollection('images');
                     }
                 }else{
                     $row->update($r);
                 }
             }else{
                 Setting::create($r);
             }
         } 
         
         return redirect()->route('admin.settings.index')->withSuccess('You have just updated the setting successfully.');
     }
 
     public function saveImage(Request $request){
         if($request->hasFile('file')){
             $path = saveTempImage($request->file('file'));
             return response()->json(['Status' => true, 'path' => $path]);
         }
         return response()->json(['Status' => false]);
     }
}
