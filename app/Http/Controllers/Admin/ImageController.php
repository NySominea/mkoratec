<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    public function saveSummernoteImage(){
        $this->validate(request(),[
            'file' => 'image|mimes:jpg,jpeg,png'
        ]);

        $path = saveSummernoteImage(request()->file('file'));

        return response()->json([   
            'url' => url('/').'/'.$path
        ]);
    }
}
