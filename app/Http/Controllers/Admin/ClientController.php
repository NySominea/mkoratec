<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Client;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::orderBy('id','DESC')->paginate(10);
        
        return view('admin.client.index',compact('clients'));
    }

    public function create()
    {
        return view('admin.client.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'required'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.clients.index')->withSuccess('You have just added a client successfully!');
    }

    public function edit($id)
    {
        $client = Client::findOrFail($id);
        return view('admin.client.add-update',compact('client'));
    }

    public function update(Request $request, $id)
    {  
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.clients.index')->withSuccess('You have just updated a client successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $client = Client::find($id);
       if($client){
           DB::beginTransaction();
           try{
               if($client->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveImage(Request $request){
       if($request->hasFile('file')){
           $path = saveTempImage($request->file('file'));
           return response()->json(['Status' => true, 'path' => $path]);
       }
       return response()->json(['Status' => false]);
   }

   public function deleteImage(){
       $result = false;
       $client_id = request()->get('client_id');
       DB::beginTransaction();
       try{
           $client = Client::findOrFail($client_id);
           if($client){
               $client->clearMediaCollection('images');
               $result = true;
           }
           DB::commit();
       }catch(Exception $exception){
           DB::rollback();
           $result = false;
       }
       
       return response()->json(['success' => $result]);
   }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $client = isset($id) ? Client::find($id) : new client;
            if(!$client) return redirect()->back()->withError('There is no record found!');

            $client->fill($data);
            if($client->save()){

               if(isset($data['image']) && !empty($data['image'])){
                   $client->clearMediaCollection('images'); 
                   $client->addMedia($data['image'])
                        ->toMediaCollection('images');
               }
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $client;
    }
}
