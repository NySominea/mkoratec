<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Partner;

class PartnerController extends Controller
{
    public function index()
    {
        $partners = Partner::orderBy('id','DESC')->paginate(10);
        
        return view('admin.partner.index',compact('partners'));
    }

    public function create()
    {
        return view('admin.partner.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'required'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.partners.index')->withSuccess('You have just added a partner successfully!');
    }

    public function edit($id)
    {
        $partner = Partner::findOrFail($id);
        return view('admin.partner.add-update',compact('partner'));
    }

    public function update(Request $request, $id)
    {  
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.partners.index')->withSuccess('You have just updated a partner successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $partner = Partner::find($id);
       if($partner){
           DB::beginTransaction();
           try{
               if($partner->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveImage(Request $request){
       if($request->hasFile('file')){
           $path = saveTempImage($request->file('file'));
           return response()->json(['Status' => true, 'path' => $path]);
       }
       return response()->json(['Status' => false]);
   }

   public function deleteImage(){
       $result = false;
       $partner_id = request()->get('partner_id');
       DB::beginTransaction();
       try{
           $partner = Partner::findOrFail($partner_id);
           if($partner){
               $partner->clearMediaCollection('images');
               $result = true;
           }
           DB::commit();
       }catch(Exception $exception){
           DB::rollback();
           $result = false;
       }
       
       return response()->json(['success' => $result]);
   }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $partner = isset($id) ? Partner::find($id) : new Partner;
            if(!$partner) return redirect()->back()->withError('There is no record found!');

            $partner->fill($data);
            if($partner->save()){

               if(isset($data['image']) && !empty($data['image'])){
                   $partner->clearMediaCollection('images'); 
                   $partner->addMedia($data['image'])
                        ->toMediaCollection('images');
               }
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $partner;
    }
}
