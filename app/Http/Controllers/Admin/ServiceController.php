<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Service;
use DB;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::orderBy('id','DESC')->paginate(10);

        return view('admin.service.index',compact('services'));
    }

    public function create()
    {
        return view('admin.service.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'required',
            'name' => 'required',
            'description' => 'required'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.services.index')->withSuccess('You have just added a service successfully!');
    }

    public function edit($id)
    {
        $service = service::findOrFail($id);
        return view('admin.service.add-update',compact('service'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required'
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.services.index')->withSuccess('You have just updated a service successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $service = Service::find($id);
       if($service){
           DB::beginTransaction();
           try{
               if($service->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveImage(Request $request){
       if($request->hasFile('file')){
           $path = saveTempImage($request->file('file'));
           return response()->json(['Status' => true, 'path' => $path]);
       }
       return response()->json(['Status' => false]);
   }

    public function deleteImage(){
        $result = false;
        $service_id = request()->get('service_id');
        DB::beginTransaction();
        try{
            $service = Service::findOrFail($service_id);
            if($service){
                $service->clearMediaCollection('images');
                $result = true;
            }
            DB::commit();
        }catch(Exception $exception){
            DB::rollback();
            $result = false;
        }

        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $service = isset($id) ? Service::find($id) : new Service;
            if(!$service) return redirect()->back()->withError('There is no record found!');

            $service->fill($data);
            if($service->save()){

               if(isset($data['image']) && !empty($data['image'])){
                   $filename = generateFileName($service->name);
                   $service->clearMediaCollection('images');
                   $service->addMedia($data['image'])
                        ->usingName($filename)
                        ->usingFileName($filename.'.'.pathinfo($data['image'], PATHINFO_EXTENSION))
                        ->toMediaCollection('images');
               }
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $service;
    }
}
