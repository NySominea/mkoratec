<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Member;
use DB;

class MemberController extends Controller
{
    public function index()
    {
        $members = Member::orderBy('id','ASC')->paginate(10);

        return view('admin.member.index',compact('members'));
    }

    public function create()
    {
        return view('admin.member.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'required',
            'name' => 'required',
            'position' => 'required',
            'message' => 'required'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.members.index')->withSuccess('You have just added a member successfully!');
    }

    public function edit($id)
    {
        $member = member::findOrFail($id);
        return view('admin.member.add-update',compact('member'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'position' => 'required',
            'message' => 'required'
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.members.index')->withSuccess('You have just updated a member successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $member = Member::find($id);
       if($member){
           DB::beginTransaction();
           try{
               if($member->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveImage(Request $request){
       if($request->hasFile('file')){
           $path = saveTempImage($request->file('file'));
           return response()->json(['Status' => true, 'path' => $path]);
       }
       return response()->json(['Status' => false]);
   }

    public function deleteImage(){
        $result = false;
        $member_id = request()->get('member_id');
        DB::beginTransaction();
        try{
            $member = Member::findOrFail($member_id);
            if($member){
                $member->clearMediaCollection('images');
                $result = true;
            }
            DB::commit();
        }catch(Exception $exception){
            DB::rollback();
            $result = false;
        }

        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $member = isset($id) ? Member::find($id) : new Member;
            if(!$member) return redirect()->back()->withError('There is no record found!');

            $member->fill($data);
            if($member->save()){
               if(isset($data['image']) && !empty($data['image'])){
                    $filename = generateFileName($member->name);
                    $member->clearMediaCollection('images');
                    $member->addMedia($data['image'])
                            ->usingName($filename)
                            ->usingFileName($filename.'.'.pathinfo($data['image'], PATHINFO_EXTENSION))
                            ->toMediaCollection('images');
               }
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $member;
    }
}
