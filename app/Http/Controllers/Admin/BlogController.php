<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Blog;
use App\Model\BlogCategory;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::orderBy('id','DESC')->paginate(10);
        
        return view('admin.blog.list.index',compact('blogs'));
    }

    public function create()
    {
        $categories = BlogCategory::orderBy('name','ASC')->pluck('name','id')->toArray();
        $categories = ['0' => 'Choose a category'] + $categories;

        return view('admin.blog.list.add-update', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.blogs.index')->withSuccess('You have just added a blog successfully!');
    }

    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        $categories = BlogCategory::orderBy('name','ASC')->pluck('name','id')->toArray();
        $categories = ['0' => 'Choose a category'] + $categories;
        return view('admin.blog.list.add-update',compact('blog','categories'));
    }

    public function update(Request $request, $id)
    {  
        $this->validate($request,[
            'title' => 'required'
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.blogs.index')->withSuccess('You have just updated a blog successfully!');
    }

    public function destroy($id)
    {
       $result = false;
       $blog = blog::find($id);
       if($blog){
           DB::beginTransaction();
           try{
               if($blog->delete()){
                   $result = true;
               }
               DB::commit();
           }catch(Exception $exception){
               DB::rollback();
               $result = false;
           }
       }
       return response()->json(['success' => $result]);
    }

    public function saveImage(Request $request){
       if($request->hasFile('file')){
           $path = saveTempImage($request->file('file'));
           return response()->json(['Status' => true, 'path' => $path]);
       }
       return response()->json(['Status' => false]);
   }

    public function deleteImage(){
        $result = false;
        $blog_id = request()->get('blog_id');
        DB::beginTransaction();
        try{
            $blog = blog::findOrFail($blog_id);
            if($blog){
                $blog->clearMediaCollection('images');
                $result = true;
            }
            DB::commit();
        }catch(Exception $exception){
            DB::rollback();
            $result = false;
        }
        
        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $blog = isset($id) ? Blog::find($id) : new Blog;
            if(!$blog) return redirect()->back()->withError('There is no record found!');

            $data['slug'] = slugify($data['title']);
            $blog->fill($data);
            if($blog->save()){

               if(isset($data['image']) && !empty($data['image'])){
                   $blog->clearMediaCollection('images'); 
                   $blog->addMedia($data['image'])
                        ->toMediaCollection('images');
               }
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $blog;
    }
}
