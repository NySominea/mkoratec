<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\PortfolioCategory;

class PortfolioCategoryController extends Controller
{
    public function index()
    {   
        $categories = PortfolioCategory::with('portfolios')->orderBy('id','ASC')->paginate(15);
        return view('admin.portfolio.category.index',compact('categories'));
    }

    public function create()
    {
        return view('admin.portfolio.category.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:portfolio_categories,name'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('admin.portfolios.categories.index')->withSuccess('You have just added a category successfully!');    
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $category = PortfolioCategory::findOrFail($id);
        
        return view('admin.portfolio.category.add-update',compact('category'));
    }

    public function update(Request $request, $id)
    {   
        $this->validate($request,[
            'name' => 'required|unique:portfolio_categories,name,'.$id
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('admin.portfolios.categories.index')->withSuccess('You have just updated a category successfully!');    
    }

    public function destroy($id)
    {
        $result = false;
        $item = PortfolioCategory::findOrFail($id);
        if($item){
            DB::beginTransaction();
            try{
                if($item->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        $category = isset($id) ? PortfolioCategory::find($id) : new PortfolioCategory;
        DB::beginTransaction();
        try{
            if(!$category) return redirect()->back()->withError('There is no record found!');
            $category->fill($data);
            $category->save();
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $category;
    }
}
