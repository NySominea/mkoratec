<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Banner;
use App\Model\Service;
use App\Model\Member;
use App\Model\Partner;
use App\Model\Client;
use App\Model\Portfolio;
use App\Model\PortfolioCategory;
use App\Model\Blog;

class FrontendController extends Controller
{
    public function homepage(){
        $constants = config()->get('constant.SETTING');
        $banners = Banner::whereState(1)->orderBy('id','DESC')->get();
        $services = Service::whereState(1)->orderBy('id','DESC')->get();
        $members = Member::whereState(1)->orderBy('id','ASC')->get();
        $partners = Partner::whereState(1)->orderBy('id','ASC')->get();
        $clients = Client::whereState(1)->orderBy('id','ASC')->get();
        $portfolios = Portfolio::whereState(1)->orderBy('id','DESC')->get();
        $portfolioCategories = PortfolioCategory::with('portfolios')->whereState(1)->get()->keyBy('id');
        $blogs = Blog::whereState(1)->orderBy('id','DESC')->get();
        return view('client.homepage.index',compact('constants','banners','services','members','partners','clients','portfolios','portfolioCategories','blogs'));
    }
}
