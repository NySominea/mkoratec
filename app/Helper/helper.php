<?php
use App\Model\Setting;

function general() {
    $settings = Setting::all()->keyBy('key');
    $constants = config()->get('constant.SETTING');

    $site_name = isset($settings['company'][$constants['SITE_NAME']]) ? $settings['company'][$constants['SITE_NAME']]->value : config('app.name');
    $site_url = isset($settings['company'][$constants['SITE_URL']]) ? $settings['company'][$constants['SITE_URL']]->value : '';
    $site_description = isset($settings['company'][$constants['COMPANY_DESCRIPTION']]) ? $settings['company'][$constants['COMPANY_DESCRIPTION']]->value : '';
    $logo = isset($settings['company'][$constants['MAIN_LOGO']]) ? $settings['company'][$constants['MAIN_LOGO']]->getFirstMedia('images') : null;
    $main_seo_description = isset($settings['company'][$constants['MAIN_SEO_DESCRIPTION']]) ? $settings['company'][$constants['MAIN_SEO_DESCRIPTION']]->value : '';
    $main_seo_keyword = isset($settings['company'][$constants['MAIN_SEO_KEYWORD']]) ? $settings['company'][$constants['MAIN_SEO_KEYWORD']]->value : '';
    $email = isset($settings['company'][$constants['EMAIL']]) ? $settings['company'][$constants['EMAIL']]->value : '';
    $phone = isset($settings['company'][$constants['PHONE']]) ? $settings['company'][$constants['PHONE']]->value : '';
    $address = isset($settings['company'][$constants['ADDRESS']]) ? $settings['company'][$constants['ADDRESS']]->value : '';
    $facebook_url = isset($settings['company'][$constants['FACEBOOK_URL']]) ? $settings['company'][$constants['FACEBOOK_URL']]->value : '';
    $instagram_url = isset($settings['company'][$constants['INSTAGRAM_URL']]) ? $settings['company'][$constants['INSTAGRAM_URL']]->value : '';
    $linkedin_url = isset($settings['company'][$constants['LINKIN_URL']]) ? $settings['company'][$constants['LINKIN_URL']]->value : '';

    return [
        'site_name' => $site_name ,
        'site_url' => $site_url,
        'logo' => $logo ? $logo->getUrl() : '',
        'site_description' => $site_description,
        'main_seo_description' => $main_seo_description,
        'main_seo_keyword' => $main_seo_keyword,
        'email' => $email,
        'phone' => $phone,
        'address' => $address,
        'facebook_url' => $facebook_url,
        'instagram_url' => $instagram_url,
        'linkedin_url' => $linkedin_url,
    ];
}

function generateFileName($name){
    $prefix = general()['site_name'];
    $name = str_replace(' ', '-', $name);
    return strtolower($prefix.'-'.$name);
}

function settings(){
    $companySettings = Setting::all()->keyBy('key');
    return ['company' => $companySettings];
}

function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function saveTempImage($image){
    $filename = time().'-'.$image->getClientOriginalName();
    $destination = 'admin/assets/images/temp';

    $image->move(public_path($destination), $filename);
    $path = $destination.'/'.$filename;

    return $path;
}

function saveSummernoteImage($image){
    $filename =time().'-'.$image->getClientOriginalExtension();
    $destination = 'admin/assets/images/summernote';

    $image->move(public_path($destination), $filename);
    $path = $destination.'/'.$filename;

    return $path;
}

function permissions(){
    return [
        'setting' => [
            'module' => 'Company Setting',
            'permissions' => [
                ['text' => 'View Company Setting', 'value' => 'view-company-setting'],
                ['text' => 'Company Setting Modification', 'value' => 'company-setting-modification']
            ]
        ],
        'banner' => [
            'module' => 'Banner',
            'permissions' => [
                ['text' => 'View Banner', 'value' => 'view-banner'],
                ['text' => 'Banner Modification', 'value' => 'banner-modification'],
                ['text' => 'Add New Banner', 'value' => 'add-new-banner'],
            ]
            ],
        'user' => [
            'module' => 'User',
            'permissions' => [
                ['text' => 'View User', 'value' => 'view-user'],
                ['text' => 'User Modification', 'value' => 'user-modification'],
                ['text' => 'Add New User', 'value' => 'add-new-user'],
                ['text' => 'View Role', 'value' => 'view-role'],
                ['text' => 'Role Modification', 'value' => 'role-modification'],
                ['text' => 'Add New Role', 'value' => 'add-new-role'],
            ]
        ]
    ];
}
