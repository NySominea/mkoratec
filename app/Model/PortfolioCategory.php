<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategory extends Model
{
    protected $fillable = ['name','state'];

    public function portfolios(){
        return $this->hasMany(Portfolio::class);
    }
}
