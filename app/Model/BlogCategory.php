<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $fillable = ['name','state'];
    
        public function blogs(){
            return $this->hasMany(Blog::class);
        }
}
