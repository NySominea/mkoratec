<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Image\Manipulations;

class Portfolio extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = ['name','description','link', 'state', 'portfolio_category_id'];

    public function category(){
        return $this->belongsTo(PortfolioCategory::class,'portfolio_category_id','id');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->format(Manipulations::FORMAT_JPG)
            ->crop(Manipulations::CROP_CENTER, 320, 320)
            ->optimize();
    }
}
