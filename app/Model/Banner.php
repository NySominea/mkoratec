<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Image\Manipulations;

class Banner extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = ['title','description','state'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->format(Manipulations::FORMAT_JPG)
            ->crop(Manipulations::CROP_CENTER, 1200, 900)
            ->quality(80)
            ->optimize();
    }
}
