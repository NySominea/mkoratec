<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;

class Blog extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $fillable = ['title','slug','description','blog_category_id'];

    public function category(){
        return $this->belongsTo(BlogCategory::class,'blog_category_id','id');
    }
}
