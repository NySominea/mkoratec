-- MySQL dump 10.17  Distrib 10.3.16-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: mkoratec
-- ------------------------------------------------------
-- Server version	10.3.16-MariaDB-1:10.3.16+maria~xenial-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,NULL,NULL,1,'2019-07-19 09:18:58','2019-07-19 09:18:58'),(2,'Mkoratec',NULL,1,'2019-07-19 09:19:09','2019-07-19 09:21:26'),(3,NULL,NULL,1,'2019-07-19 09:19:18','2019-07-19 09:19:18'),(4,NULL,NULL,1,'2019-07-19 09:19:29','2019-07-19 09:19:29');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_categories`
--

DROP TABLE IF EXISTS `blog_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_categories`
--

LOCK TABLES `blog_categories` WRITE;
/*!40000 ALTER TABLE `blog_categories` DISABLE KEYS */;
INSERT INTO `blog_categories` VALUES (1,'Technology',1,'2019-07-19 09:39:25','2019-07-19 09:39:25');
/*!40000 ALTER TABLE `blog_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `blog_category_id` tinyint(3) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blogs_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
INSERT INTO `blogs` VALUES (1,'New Technology on the Block','new-technology-on-the-block','<p><span style=\"color: rgb(0, 20, 35); font-family: freight-text-pro, serif; font-size: 18px;\">In a global economy with increasingly far-flung workforces, companies often need to pay contract workers in other countries. They can do it the conventional way, involving processing an invoice and either cutting a check or collecting and managing direct-deposit data. Along the way, currency has to be converted, meaning a middleman is paid a percentage to make the exchange. Or companies can pay in bitcoin. The payment with the digital currency is immediate, and if the worker lives in a country such as India that has a bitcoin exchange, there are minimal fees to convert. Compared with conventional banking, it is simpler, cheaper and faster.</span></p><p><img src=\"https://mkoratec.com/admin/assets/images/summernote/1563504115-jpg\" style=\"width: 543px;\"><span style=\"color: rgb(0, 20, 35); font-family: freight-text-pro, serif; font-size: 18px;\"><br></span></p>',1,1,'2019-07-19 09:42:03','2019-07-19 09:42:03');
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'Big Biz','http://bigbizgroups.com',1,'2019-07-19 09:36:08','2019-07-19 09:36:08'),(3,'Ministry of Toursim','https://www.facebook.com/cambodiamot/',1,'2019-07-19 11:31:53','2019-07-20 14:19:02'),(4,'Cambodia Alumni Association','https://caacambodia.org/',1,'2019-07-19 11:33:17','2019-07-19 11:33:17'),(5,'Stormy Delivery','https://www.facebook.com/stormydelivery',1,'2019-07-19 11:33:43','2019-07-20 14:17:47'),(6,'Nolynn','https://www.youtube.com/channel/UC1tnH6XKVLXkbpu5H6h4yPw',1,'2019-07-19 11:34:00','2019-07-20 14:16:48'),(7,'YOUADME','https://www.facebook.com/YouAdMeKH/?epa=SEARCH_BOX',1,'2019-08-09 22:55:29','2019-08-09 22:55:29');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsive_images` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (1,'App\\User',1,'images','1563502431-1563354198-61109944_2119578928152178_6652194606748270592_n','1563502431-1563354198-61109944_2119578928152178_6652194606748270592_n.jpg','image/jpeg','public',171392,'[]','[]','[]',1,'2019-07-19 09:13:57','2019-07-19 09:13:57'),(2,'App\\Model\\Banner',1,'images','1563502729-mkoratec_slide_1','1563502729-mkoratec_slide_1.jpg','image/jpeg','public',773531,'[]','[]','[]',2,'2019-07-19 09:18:58','2019-07-19 09:18:58'),(3,'App\\Model\\Banner',2,'images','1563502748-mkoratec_slide_2','1563502748-mkoratec_slide_2.jpg','image/jpeg','public',969148,'[]','[]','[]',3,'2019-07-19 09:19:09','2019-07-19 09:19:09'),(4,'App\\Model\\Banner',3,'images','1563502757-mkoratec_slide_3','1563502757-mkoratec_slide_3.jpg','image/jpeg','public',984980,'[]','[]','[]',4,'2019-07-19 09:19:18','2019-07-19 09:19:18'),(5,'App\\Model\\Banner',4,'images','1563502768-mkoratec_slide_4','1563502768-mkoratec_slide_4.jpg','image/jpeg','public',774612,'[]','[]','[]',5,'2019-07-19 09:19:29','2019-07-19 09:19:29'),(7,'App\\Model\\Setting',6,'images','1563503042-logo','1563503042-logo.png','image/png','public',597295,'[]','[]','[]',7,'2019-07-19 09:24:21','2019-07-19 09:24:21'),(8,'App\\Model\\Setting',7,'images','1563503025-nontext-logo','1563503025-nontext-logo.png','image/png','public',760089,'[]','[]','[]',8,'2019-07-19 09:24:21','2019-07-19 09:24:21'),(9,'App\\Model\\Setting',8,'images','1563503037-text-logo','1563503037-text-logo.png','image/png','public',184254,'[]','[]','[]',9,'2019-07-19 09:24:21','2019-07-19 09:24:21'),(13,'App\\Model\\Blog',1,'images','1563504098-blockchain','1563504098-blockchain.jpg','image/jpeg','public',1661167,'[]','[]','[]',12,'2019-07-19 09:42:03','2019-07-19 09:42:03'),(17,'App\\Model\\Client',3,'images','1563510703-MOT_logo_png','1563510703-MOT_logo_png.png','image/png','public',339440,'[]','[]','[]',16,'2019-07-19 11:31:53','2019-07-19 11:31:53'),(25,'App\\Model\\Member',5,'images','1563552903-photo_2019-07-14_00-01-05','1563552903-photo_2019-07-14_00-01-05.jpg','image/jpeg','public',127980,'[]','[]','[]',24,'2019-07-19 23:15:33','2019-07-19 23:15:33'),(27,'App\\Model\\Member',6,'images','1563553259-61097821_2119578891485515_7486901597847420928_n','1563553259-61097821_2119578891485515_7486901597847420928_n.jpg','image/jpeg','public',171392,'[]','[]','[]',26,'2019-07-19 23:21:01','2019-07-19 23:21:01'),(29,'App\\Model\\Member',8,'images','1563553471-photo_2019-07-19_23-03-05','1563553471-photo_2019-07-19_23-03-05.jpg','image/jpeg','public',98609,'[]','[]','[]',28,'2019-07-19 23:24:34','2019-07-19 23:24:34'),(30,'App\\Model\\Member',9,'images','1563553781-photo_2019-05-26_13-25-51','1563553781-photo_2019-05-26_13-25-51.jpg','image/jpeg','public',237864,'[]','[]','[]',29,'2019-07-19 23:29:47','2019-07-19 23:29:47'),(31,'App\\Model\\Member',4,'images','1563556374-sophal','1563556374-sophal.png','image/png','public',925587,'[]','[]','[]',30,'2019-07-20 00:12:56','2019-07-20 00:12:56'),(32,'App\\Model\\Setting',5,'images','1563602902-MkoraTec_Secondary_logo_white final','1563602902-MkoraTec_Secondary_logo_white-final.png','image/png','public',632309,'[]','[]','[]',31,'2019-07-20 13:08:25','2019-07-20 13:08:25'),(34,'App\\Model\\Portfolio',2,'images','1563604367-Mow mockup-02','1563604367-Mow-mockup-02.jpg','image/jpeg','public',252307,'[]','[]','[]',32,'2019-07-20 13:32:54','2019-07-20 13:32:54'),(35,'App\\Model\\Client',6,'images','1563606976-nolynn-01','1563606976-nolynn-01.png','image/png','public',31844,'[]','[]','[]',33,'2019-07-20 14:16:21','2019-07-20 14:16:21'),(36,'App\\Model\\Client',5,'images','1563607040-Stormy delivery-01','1563607040-Stormy-delivery-01.png','image/png','public',60252,'[]','[]','[]',34,'2019-07-20 14:17:47','2019-07-20 14:17:47'),(37,'App\\Model\\Client',4,'images','1563607074-CAA logo-01','1563607074-CAA-logo-01.png','image/png','public',62795,'[]','[]','[]',35,'2019-07-20 14:17:56','2019-07-20 14:17:56'),(39,'App\\Model\\Client',1,'images','1563607814-Webp.net-resizeimage','1563607814-Webp.net-resizeimage.png','image/png','public',13958,'[]','[]','[]',36,'2019-07-20 14:30:16','2019-07-20 14:30:16'),(40,'App\\Model\\Partner',2,'images','1563608639-Asset 1','1563608639-Asset-1.png','image/png','public',344692,'[]','[]','[]',37,'2019-07-20 14:44:04','2019-07-20 14:44:04'),(41,'App\\Model\\Portfolio',3,'images','1563609037-moon studio moc-2','1563609037-moon-studio-moc-2.jpg','image/jpeg','public',456073,'[]','[]','[]',38,'2019-07-20 14:51:29','2019-07-20 14:51:29'),(42,'App\\Model\\Portfolio',4,'images','1563609608-mot','1563609608-mot.jpg','image/jpeg','public',733788,'[]','[]','[]',39,'2019-07-20 15:00:46','2019-07-20 15:00:46'),(43,'App\\Model\\Service',1,'images','1563610858-service_ux_ui_design','1563610858-service_ux_ui_design.png','image/png','public',8785,'[]','[]','[]',40,'2019-07-20 15:21:00','2019-07-20 15:21:00'),(44,'App\\Model\\Service',2,'images','1563611063-service_creative_design','1563611063-service_creative_design.png','image/png','public',3993,'[]','[]','[]',41,'2019-07-20 15:24:24','2019-07-20 15:24:24'),(45,'App\\Model\\Service',3,'images','1563611366-service_marketing','1563611366-service_marketing.png','image/png','public',3692,'[]','[]','[]',42,'2019-07-20 15:29:28','2019-07-20 15:29:28'),(46,'App\\Model\\Service',4,'images','1563611584-service_dev','1563611584-service_dev.png','image/png','public',3623,'[]','[]','[]',43,'2019-07-20 15:33:05','2019-07-20 15:33:05'),(47,'App\\Model\\Member',7,'images','1563612002-photo_2019-07-20_15-39-29','1563612002-photo_2019-07-20_15-39-29.jpg','image/jpeg','public',186816,'[]','[]','[]',44,'2019-07-20 15:40:06','2019-07-20 15:40:06'),(48,'App\\Model\\Portfolio',5,'images','1563612835-mockup-03','1563612835-mockup-03.jpg','image/jpeg','public',685453,'[]','[]','[]',45,'2019-07-20 15:53:57','2019-07-20 15:53:57'),(49,'App\\Model\\Portfolio',6,'images','1563613087-mockup-02','1563613087-mockup-02.jpg','image/jpeg','public',369597,'[]','[]','[]',46,'2019-07-20 15:58:09','2019-07-20 15:58:09'),(52,'App\\Model\\Client',7,'images','1565366113-65429472_2344152679172137_745989248984809472_o','1565366113-65429472_2344152679172137_745989248984809472_o.png','image/png','public',137108,'[]','[]','[]',48,'2019-08-09 22:55:29','2019-08-09 22:55:29'),(53,'App\\Model\\Portfolio',7,'images','1565450313-Untitled-2-01-01','1565450313-Untitled-2-01-01.png','image/png','public',619414,'[]','[]','[]',49,'2019-08-10 22:18:35','2019-08-10 22:18:35'),(54,'App\\Model\\Portfolio',8,'images','1565450859-di store-01-01','1565450859-di-store-01-01.png','image/png','public',37056,'[]','[]','[]',50,'2019-08-10 22:27:43','2019-08-10 22:27:43');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (4,'LENG Sophal','Chief Execute Officer','Success isn’t about how much you earn, but how you make people trust on your integrity',1,'2019-07-19 23:07:14','2019-07-19 23:16:37'),(5,'ENG Kimhour','Chief Technology Officer','I have knowledge and experience about technology more than 3 years. I have a lot of knowledge about developing website application, mobile application and system application. I always keep in touch to learn about new technology to build my ability.',1,'2019-07-19 23:15:33','2019-07-19 23:15:33'),(6,'NY Sominea','Web Application Developer','I am a highly competent web application developer with 3 years’ experience developing high performance products for clients. I am always fascinated on updating my backend knowledge with trendy technologies and delivering the best quality of products toward my clients.',1,'2019-07-19 23:21:01','2019-07-19 23:21:01'),(7,'KEO Pichyvoin','Chief Digital Officer','Experienced strategist with demonstrated history of working background in marketing, advertising and development industry. Strong professional business development skills in Digital Strategy and Marketing, User Experience, E-commerce, Integrating Marketing, and Business Transformation.',1,'2019-07-19 23:23:52','2019-07-20 15:40:06'),(8,'RA Nireach','Senior Graphic Designer','Design is simple, that’s why it’s so complicated',1,'2019-07-19 23:24:34','2019-07-19 23:31:15'),(9,'LENG Chamnan','Administrative Operation Officer','Well prepare is my priority to make a decision.',1,'2019-07-19 23:29:47','2019-07-19 23:33:38');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_06_17_020002_create_banners_table',1),(4,'2019_06_17_020214_create_services_table',1),(5,'2019_06_17_020324_create_portfolios_table',1),(6,'2019_06_17_020402_create_portfolio_categories_table',1),(7,'2019_06_17_020639_create_partners_table',1),(8,'2019_06_17_020718_create_clients_table',1),(9,'2019_06_17_020756_create_blogs_table',1),(10,'2019_06_17_021234_create_settings_table',1),(11,'2019_06_17_121429_create_media_table',1),(12,'2019_06_17_121647_create_permission_tables',1),(13,'2019_06_19_114913_create_members_table',1),(14,'2019_06_19_223800_create_blog_categories_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\User',1);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (2,'OOM','https://www.facebook.com/oommedia/',1,'2019-07-19 17:05:38','2019-07-20 15:36:42');
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'view-company-setting','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(2,'company-setting-modification','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(3,'view-banner','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(4,'banner-modification','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(5,'add-new-banner','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(6,'view-user','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(7,'user-modification','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(8,'add-new-user','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(9,'view-role','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(10,'role-modification','web','2019-07-13 14:38:21','2019-07-13 14:38:21'),(11,'add-new-role','web','2019-07-13 14:38:21','2019-07-13 14:38:21');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_categories`
--

DROP TABLE IF EXISTS `portfolio_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_categories`
--

LOCK TABLES `portfolio_categories` WRITE;
/*!40000 ALTER TABLE `portfolio_categories` DISABLE KEYS */;
INSERT INTO `portfolio_categories` VALUES (1,'Web Development',1,'2019-07-19 09:52:09','2019-07-19 09:52:09'),(2,'Creative Design',1,'2019-07-20 13:10:04','2019-07-20 13:10:21');
/*!40000 ALTER TABLE `portfolio_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolios`
--

DROP TABLE IF EXISTS `portfolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolios` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `portfolio_category_id` tinyint(3) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolios`
--

LOCK TABLES `portfolios` WRITE;
/*!40000 ALTER TABLE `portfolios` DISABLE KEYS */;
INSERT INTO `portfolios` VALUES (2,'MOW Milk','Mow aims to fulfill the dream of all Cambodian with the best fresh milk and quality. We are absolutely proud to be a part of Mow’s project.','https://www.facebook.com/2250187675066697/posts/2368282303257233?s=100002974173962&sfns=mo',1,2,'2019-07-20 13:32:35','2019-07-20 14:52:21'),(3,'MOON STUDIO','Wan to build your best memory ? Moon Studio is the best choice for you, set up yours memory now with Moon Studio.',NULL,1,2,'2019-07-20 14:51:29','2019-07-20 15:58:45'),(4,'Digital Drawing and Animation ( Ministry of Tourism )','Digital Character and Animation PROJECT: ក្រមប្រតិបត្តិ និងក្រមវិជ្ជាជីវ:មគុទេសក៍ទេសចរណ៍កម្ពុជា។','https://www.facebook.com/2250187675066697/posts/2334135690005228?s=100002974173962&sfns=mo',1,2,'2019-07-20 15:00:46','2019-07-20 15:58:39'),(5,'Cambodia Alumni Association',NULL,'https://caacambodia.org/',1,1,'2019-07-20 15:53:57','2019-07-20 15:53:57'),(6,'BIGBIZ groups',NULL,'http://bigbizgroups.com',1,1,'2019-07-20 15:58:09','2019-07-20 15:58:19'),(7,'CGI animation of YOUADME project Promote','សូមថ្លែងអំណរគុណដល់ក្រុមហ៊ុន YouAdMe ដែលបានសហការក្នុងការផលិត វីដេអូ Animation​ ជាមួយ MkoraTec យើងខ្ញុំ។\r\n\r\nThanks to #YouAdMe for corporate producing video Animation with #MkoraTec​.','https://www.facebook.com/2250187675066697/posts/2413372745414855?s=100002974173962&sfns=mo',1,2,'2019-08-09 22:51:01','2019-08-09 22:51:01'),(8,'Dii Store','Dii store aims to provide original skincare/cosmetic product with a special price where you can find in town.\r\nPlease fully support us, we will bring you the best service and product.','https://www.facebook.com/Diithestore/photos/a.1410145029374186/1422640168124672/?type=3',1,2,'2019-08-10 22:27:43','2019-08-10 22:28:07');
/*!40000 ALTER TABLE `portfolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Super Administrator','web','2019-07-13 14:38:21','2019-07-13 14:38:21');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'UX | UI Design','We re-search and design prototype of Web and Mobile Apps, Web portals and landing pages \r\nWe accept every design request and every detail you need to match your desire.',1,'2019-07-20 15:21:00','2019-07-20 15:21:00'),(2,'CREATIVE DESIGN','With our Creative and young team, we guaranty your branding is stand-out.\r\nWe accept every design request and every detail you need a few option to choose.',1,'2019-07-20 15:24:24','2019-07-20 15:24:24'),(3,'DIGITAL MARKETING','Our propriety MKORATEC provide immediate and unmatched campaign intelligence. This enable us to spend more time in the idea stage, where we can optimize you campaign, help you find new areas of opportunity and create more impact consumer engagement.',1,'2019-07-20 15:29:28','2019-07-20 15:29:28'),(4,'SOFTWARE DEVELOPMENT','For bring pixel-prefect Design we prefer converting our mock-up into Web or Mobile Apps using front-end/Back-end development tools and programming languages.',1,'2019-07-20 15:33:05','2019-07-20 15:33:05');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site_name','Mkoratec',1,'2019-07-13 14:39:42','2019-07-13 23:02:41'),(2,'site_url','https://www.mkoratec.com/',1,'2019-07-13 14:39:42','2019-07-14 00:06:43'),(3,'company_description','𝙈𝙠𝙤𝙧𝙖𝙏𝙚𝙘 is the trust company which bring you quality of products such as: 𝐖𝐞𝐛 𝐃𝐞𝐯𝐞𝐥𝐨𝐩𝐦𝐞𝐧𝐭, 𝐌𝐨𝐛𝐢𝐥𝐞 𝐀𝐩𝐩𝐥𝐢𝐜𝐚𝐭𝐢𝐨𝐧, 𝐃𝐢𝐠𝐢𝐭𝐚𝐥 𝐌𝐚𝐫𝐤𝐞𝐭𝐢𝐧𝐠, and 𝐔𝐗/𝐔𝐈 & 𝐆𝐫𝐚𝐩𝐡𝐢𝐜 𝐃𝐞𝐬𝐢𝐠𝐧',1,'2019-07-13 14:39:42','2019-07-13 23:44:37'),(4,'copy_right','',1,'2019-07-13 14:39:42','2019-07-13 14:39:42'),(5,'main_logo','admin/assets/images/temp/1563602902-MkoraTec_Secondary_logo_white final.png',1,'2019-07-13 14:39:42','2019-07-20 13:08:25'),(6,'secondary_logo','admin/assets/images/temp/1563503042-logo.png',1,'2019-07-13 14:39:42','2019-07-19 09:24:21'),(7,'non_text_logo','admin/assets/images/temp/1563503025-nontext-logo.png',1,'2019-07-13 14:39:42','2019-07-19 09:24:21'),(8,'text_only_logo','admin/assets/images/temp/1563503037-text-logo.png',1,'2019-07-13 14:39:42','2019-07-19 09:24:21'),(9,'seo_keyword','𝙈𝙠𝙤𝙧𝙖𝙏𝙚𝙘 is the trust company which bring you quality of products such as: 𝐖𝐞𝐛 𝐃𝐞𝐯𝐞𝐥𝐨𝐩𝐦𝐞𝐧𝐭, 𝐌𝐨𝐛𝐢𝐥𝐞 𝐀𝐩𝐩𝐥𝐢𝐜𝐚𝐭𝐢𝐨𝐧, 𝐃𝐢𝐠𝐢𝐭𝐚𝐥 𝐌𝐚𝐫𝐤𝐞𝐭𝐢𝐧𝐠, and 𝐔𝐗/𝐔𝐈 & 𝐆𝐫𝐚𝐩𝐡𝐢𝐜 𝐃𝐞𝐬𝐢𝐠𝐧',1,'2019-07-13 14:39:42','2019-07-20 00:18:04'),(10,'seo_description','',1,'2019-07-13 14:39:42','2019-07-20 00:18:04'),(11,'facebook_url','https://www.facebook.com/mkoratec/',1,'2019-07-13 14:39:42','2019-07-13 23:21:28'),(12,'instagram_url','',1,'2019-07-13 14:39:42','2019-07-13 14:39:42'),(13,'linkin_url','https://www.linkedin.com/company/14756773',1,'2019-07-13 14:39:42','2019-07-20 15:43:45'),(14,'address','St. 58P, Dei Thmei, Phnom Penh Thmei, Sen Sok, Phnom Penh 12406, Cambodia',1,'2019-07-13 14:39:42','2019-07-13 23:21:28'),(15,'email','info@mkoratec.com',1,'2019-07-13 14:39:42','2019-07-13 23:21:28'),(16,'phone','(+855) 96 888 2655',1,'2019-07-13 14:39:42','2019-07-13 23:21:28');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Developer','developer@mkoratec.com',NULL,'$2y$10$HxAa1lT1TMLxLHYxro.6MuKyQzEZl9yV4SpvyUavsgoWID6aIM2UW',1,'bV6OnWDPh9CEUYA9MkI1j84kWgD1FerhRZa16e0DluhDp3SADwG5Nvuav5RC','2019-07-13 14:38:21','2019-07-13 14:38:21');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-20  1:52:56
