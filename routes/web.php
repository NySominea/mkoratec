<?php

Route::namespace('Admin')->group(function(){
    Route::namespace('Auth')->group(function(){
        Route::get('/login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('/login', ['uses' => 'LoginController@login', 'as' => 'postLogin']);
        Route::post('/logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });
    
    Route::group(['middleware' => 'auth', 'prefix' => 'admin','as' => 'admin.'], function(){
        Route::get('/dashboard',['uses' => 'DashboardController@index', 'as' => 'dashboard']);
        Route::resource('/banners','BannerController');
        Route::resource('/services','ServiceController');
        Route::resource('/members','MemberController');
        Route::resource('/partners','PartnerController');
        Route::resource('/clients','ClientController');
        Route::resource('/blogs/categories','BlogCategoryController',['as' => 'blogs']);
        Route::resource('/blogs','BlogController');
        Route::resource('/portfolios/categories','PortfolioCategoryController',['as' => 'portfolios']);
        Route::resource('/portfolios','PortfolioController');
        Route::resource('/users/roles','RoleController',['as' => 'users']);
        Route::resource('/users','UserController');
        Route::resource('/settings','SettingController');

        // Save Image
        Route::post('/save-profile-image', ['as' => 'users.saveImage', 'uses' => 'UserController@saveImage']);
        Route::post('/save-banner-image', ['as' => 'banners.saveImage', 'uses' => 'BannerController@saveImage']);
        Route::post('/save-service-image', ['as' => 'services.saveImage', 'uses' => 'ServiceController@saveImage']);
        Route::post('/save-member-image', ['as' => 'members.saveImage', 'uses' => 'MemberController@saveImage']);
        Route::post('/save-partner-image', ['as' => 'partners.saveImage', 'uses' => 'PartnerController@saveImage']);
        Route::post('/save-client-image', ['as' => 'clients.saveImage', 'uses' => 'ClientController@saveImage']);
        Route::post('/save-portfolio-image', ['as' => 'portfolios.saveImage', 'uses' => 'PortfolioController@saveImage']);
        Route::post('/save-blog-image', ['as' => 'blogs.saveImage', 'uses' => 'BlogController@saveImage']);
        Route::post('/save-summernote-image',['as' => 'summernote.saveImage', 'uses' => 'ImageController@saveSummernoteImage']);
        Route::post('/save-image', ['as' => 'settings.saveImage', 'uses' => 'SettingController@saveImage']);
    });  
});

Route::namespace('Client')->group(function(){
    Route::get('/',['uses' => 'FrontendController@homepage','as' => 'client.homepage']);
});
