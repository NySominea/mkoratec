-- MySQL dump 10.17  Distrib 10.3.16-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: caa
-- ------------------------------------------------------
-- Server version	10.3.16-MariaDB-1:10.3.16+maria~xenial-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'{\"en\":\"WHO WE ARE\",\"kh\":\"WHO WE ARE\",\"zh\":\"WHO WE ARE\"}','{\"en\":null,\"kh\":null,\"zh\":null}',NULL,'2019-07-22 21:48:40','2019-07-29 23:03:20');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'{\"en\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit KH\",\"kh\":null,\"zh\":null}','{\"en\":\"\\\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\\\"\",\"kh\":null,\"zh\":null}','2019-07-22 22:29:33','2019-07-22 22:29:33'),(3,'{\"en\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit KH\",\"kh\":null,\"zh\":null}','{\"en\":\"\\\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\\\"\",\"kh\":null,\"zh\":null}','2019-07-22 22:29:50','2019-07-22 22:29:50'),(4,'{\"en\":\"Lorem Ipsum is simply dummy\",\"kh\":null,\"zh\":null}','{\"en\":\"<p>\\\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\\\"<\\/p><p><img src=\\\"https:\\/\\/caacambodia.org\\/app-assets\\/images\\/summernote\\/1563809510-jpg\\\" style=\\\"width: 100%;\\\"><br><\\/p>\",\"kh\":null,\"zh\":null}','2019-07-22 22:32:13','2019-07-22 22:32:13');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province_id` tinyint(3) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Serey Pheab High School',9,'2019-07-22 23:13:57','2019-07-22 23:13:57'),(2,'Kampong Cham High School',4,'2019-07-22 23:25:55','2019-07-22 23:25:55');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_lines`
--

DROP TABLE IF EXISTS `language_lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_lines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `language_lines_group_index` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_lines`
--

LOCK TABLES `language_lines` WRITE;
/*!40000 ALTER TABLE `language_lines` DISABLE KEYS */;
INSERT INTO `language_lines` VALUES (1,'client','home_menu','{\"en\":\"Home\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(2,'client','donate_menu','{\"en\":\"Donate\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(3,'client','event_menu','{\"en\":\"Event\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(4,'client','gallery_menu','{\"en\":\"Gallery\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(5,'client','register_menu','{\"en\":\"Register\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(6,'client','about_menu','{\"en\":\"About Us\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(7,'client','view_more_button','{\"en\":\"View More\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(8,'client','learn_more_button','{\"en\":\"Learn More\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(9,'client','register_button','{\"en\":\"Register\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(10,'client','discard_button','{\"en\":\"Discard\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(11,'client','select_button','{\"en\":\"Select\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(12,'client','vision_label','{\"en\":\"Our Visions\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(13,'client','mission_label','{\"en\":\"Our Missions\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(14,'client','core_value_label','{\"en\":\"Our Core Values\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(15,'client','event_label','{\"en\":\"Our Events\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(16,'client','latest_event_label','{\"en\":\"Latest Events\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(17,'client','gallery_label','{\"en\":\"Our Galleries\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(18,'client','partner_label','{\"en\":\"Our Partners\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(19,'client','story_label','{\"en\":\"Our Story\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(20,'client','member_label','{\"en\":\"Our Members\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(21,'client','member_registration_label','{\"en\":\"Member Registration\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(22,'client','short_biography_label','{\"en\":\"Short Biography\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(23,'client','today_view_label','{\"en\":\"Today View\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(24,'client','account_name_label','{\"en\":\"Account Name\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(25,'client','account_number_label','{\"en\":\"Account Number\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(26,'client','no_content_label','{\"en\":\"No available content\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(27,'client','all_album_label','{\"en\":\"All Photo Album\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(28,'client','all_album_in_province','{\"en\":\"All Photo Album in :province\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(29,'client','all_photo_of_location_in_province','{\"en\":\"All Photo of :location in :province\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(30,'client','province_placeholder','{\"en\":\"Please select a province\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(31,'client','term_condition_label','{\"en\":\"Term & Condition\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(32,'client','kh_name_label','{\"en\":\"Khmer Name\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(33,'client','en_name_label','{\"en\":\"English Name\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(34,'client','gender_label','{\"en\":\"Gender\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(35,'client','male_label','{\"en\":\"Male\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(36,'client','female_label','{\"en\":\"Female\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(37,'client','age_label','{\"en\":\"Age\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(38,'client','id_number_label','{\"en\":\"ID Number\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(39,'client','address_label','{\"en\":\"Address\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(40,'client','education_label','{\"en\":\"Education\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(41,'client','language_label','{\"en\":\"Language\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(42,'client','career_position_label','{\"en\":\"Career Position\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(43,'client','phone_label','{\"en\":\"Phone Number\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(44,'client','email_label','{\"en\":\"Email\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(45,'client','facebook_label','{\"en\":\"Facebook\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(46,'client','kh_name_placeholder','{\"en\":\"Please enter your Khmer name\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(47,'client','en_name_placeholder','{\"en\":\"Please enter your English name\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(48,'client','age_placeholder','{\"en\":\"Please enter your age\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(49,'client','id_number_placeholder','{\"en\":\"Please enter your ID number\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(50,'client','address_placeholder','{\"en\":\"Please enter your address\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(51,'client','education_placeholder','{\"en\":\"Please select your education\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(52,'client','language_placeholder','{\"en\":\"Please select your languages\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(53,'client','career_position_placeholder','{\"en\":\"Please enter your career position\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(54,'client','phone_placeholder','{\"en\":\"Please enter your phone number\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(55,'client','email_placeholder','{\"en\":\"Please enter your email address\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(56,'client','facebook_placeholder','{\"en\":\"Please enter your Facebook name\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(57,'client','optional_label','{\"en\":\"Optional\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(58,'client','against_humanity_sentence','{\"en\":\"Are you engaging in any locally or internationally movements or activities against humanity?\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(59,'client','yes_against_humanity_label','{\"en\":\"Yes, I am\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(60,'client','no_against_humanity_label','{\"en\":\"No, I am not\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(61,'client','register_condition_first_sentence','{\"en\":\"I guarantee and certify that I do not take on the role of an Associated Member or an Act to serve any political party\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(62,'client','register_condition_second_sentence','{\"en\":\"I guarantee and certify that I do not take on the role of an Associated Member or an Act to serve any political party\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(63,'client','kh_name_field_validation','{\"en\":\"The Khmer name field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(64,'client','en_name_field_validation','{\"en\":\"The English name field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(65,'client','age_field_validation','{\"en\":\"The age field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(66,'client','email_field_validation','{\"en\":\"The email field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(67,'client','phone_field_validation','{\"en\":\"The phone field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(68,'client','address_field_validation','{\"en\":\"The address field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(69,'client','education_field_validation','{\"en\":\"The education field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(70,'client','id_number_field_validation','{\"en\":\"The ID number field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(71,'client','language_field_validation','{\"en\":\"The language field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(72,'client','career_position_field_validation','{\"en\":\"The career position field is required\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(73,'client','fill_information_below_validation','{\"en\":\"Please fill all information in the form below.\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(74,'client','agree_condition_below_validation','{\"en\":\"Please agree the term condition below.\"}','2019-07-26 09:58:17','2019-08-09 13:30:36'),(75,'client','photo_4x6','{\"en\":\"Photo 4x6\"}','2019-08-09 13:30:36','2019-08-09 13:30:36'),(76,'client','choose_photo_label','{\"en\":\"Choose Photo\"}','2019-08-09 13:30:36','2019-08-09 13:30:36'),(77,'client','photo_field_validation','{\"en\":\"The photo field is required\"}','2019-08-09 13:30:36','2019-08-09 13:30:36');
/*!40000 ALTER TABLE `language_lines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `custom_properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `responsive_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (4,'App\\Model\\Setting',8,'images','1563806600-img-our-member','1563806600-img-our-member.jpg','image/jpeg','public',654972,'[]','[]','[]',3,'2019-07-22 21:43:23','2019-07-22 21:43:23'),(5,'App\\Model\\Setting',9,'images','1563806567-img-page-header-2','1563806567-img-page-header-2.jpg','image/jpeg','public',293270,'[]','[]','[]',4,'2019-07-22 21:43:23','2019-07-22 21:43:23'),(6,'App\\Model\\Setting',10,'images','1563806574-img-page-header-1','1563806574-img-page-header-1.jpg','image/jpeg','public',222026,'[]','[]','[]',5,'2019-07-22 21:43:23','2019-07-22 21:43:23'),(7,'App\\Model\\Setting',11,'images','1563806587-img-page-header-3','1563806587-img-page-header-3.jpg','image/jpeg','public',226365,'[]','[]','[]',6,'2019-07-22 21:43:23','2019-07-22 21:43:23'),(8,'App\\Model\\Setting',12,'images','1563806580-img-page-header-3','1563806580-img-page-header-3.jpg','image/jpeg','public',226365,'[]','[]','[]',7,'2019-07-22 21:43:23','2019-07-22 21:43:23'),(11,'App\\Model\\Event',1,'images','1563809345-img-mission-1','1563809345-img-mission-1.jpg','image/jpeg','public',264881,'[]','[]','[]',10,'2019-07-22 22:29:33','2019-07-22 22:29:33'),(12,'App\\Model\\Event',3,'images','1563809389-img-mission-1','1563809389-img-mission-1.jpg','image/jpeg','public',264881,'[]','[]','[]',11,'2019-07-22 22:29:50','2019-07-22 22:29:50'),(13,'App\\Model\\Event',4,'images','1563809531-img-mission-2','1563809531-img-mission-2.jpg','image/jpeg','public',239392,'[]','[]','[]',12,'2019-07-22 22:32:13','2019-07-22 22:32:13'),(18,'App\\Model\\Member',2,'images','1563809674-1563354198-61109944_2119578928152178_6652194606748270592_n','1563809674-1563354198-61109944_2119578928152178_6652194606748270592_n.jpg','image/jpeg','public',171392,'[]','[]','[]',17,'2019-07-22 22:34:35','2019-07-22 22:34:35'),(19,'App\\Model\\Gallery',1,'images','1563812013-gallery','1563812013-gallery.jpg','image/jpeg','public',264881,'[]','[]','[]',18,'2019-07-22 23:13:57','2019-07-22 23:13:57'),(20,'App\\Model\\Gallery',1,'album','1563812033-gallery','1563812033-gallery.jpg','image/jpeg','public',264881,'[]','[]','[]',19,'2019-07-22 23:13:57','2019-07-22 23:13:57'),(21,'App\\Model\\Gallery',1,'album','1563812033-img-mission-1','1563812033-img-mission-1.jpg','image/jpeg','public',264881,'[]','[]','[]',20,'2019-07-22 23:13:57','2019-07-22 23:13:57'),(22,'App\\Model\\Gallery',1,'album','1563812035-img-mission-2','1563812035-img-mission-2.jpg','image/jpeg','public',239392,'[]','[]','[]',21,'2019-07-22 23:13:57','2019-07-22 23:13:57'),(23,'App\\Model\\Gallery',2,'images','1563812735-img-mission-2','1563812735-img-mission-2.jpg','image/jpeg','public',239392,'[]','[]','[]',22,'2019-07-22 23:25:55','2019-07-22 23:25:55'),(24,'App\\Model\\Gallery',2,'album','1563812751-gallery','1563812751-gallery.jpg','image/jpeg','public',264881,'[]','[]','[]',23,'2019-07-22 23:25:55','2019-07-22 23:25:55'),(25,'App\\Model\\Gallery',2,'album','1563812751-img-mission-1','1563812751-img-mission-1.jpg','image/jpeg','public',264881,'[]','[]','[]',24,'2019-07-22 23:25:55','2019-07-22 23:25:55'),(26,'App\\Model\\Gallery',2,'album','1563812754-img-mission-2','1563812754-img-mission-2.jpg','image/jpeg','public',239392,'[]','[]','[]',25,'2019-07-22 23:25:55','2019-07-22 23:25:55'),(27,'App\\User',1,'images','1563862649-CAA logo-01','1563862649-CAA-logo-01.png','image/png','public',410943,'[]','[]','[]',26,'2019-07-23 13:17:31','2019-07-23 13:17:31'),(30,'App\\Model\\Banner',1,'images','1564416197-rsz_img-slide-1','1564416197-rsz_img-slide-1.jpg','image/jpeg','public',102307,'[]','[]','[]',28,'2019-07-29 23:03:20','2019-07-29 23:03:20'),(33,'App\\Model\\Setting',22,'images','1564455449-caa-sm','1564455449-caa-sm.gif','image/gif','public',596630,'[]','[]','[]',31,'2019-07-30 09:57:44','2019-07-30 09:57:44'),(35,'App\\Model\\Member',1,'images','1564455854-rsz_stormy_delivery-01','1564455854-rsz_stormy_delivery-01.png','image/png','public',96374,'[]','[]','[]',33,'2019-07-30 10:04:15','2019-07-30 10:04:15'),(38,'App\\Model\\Partner',3,'images','1564457717-rsz_rsz_caa_logo-01','1564457717-rsz_rsz_caa_logo-01.png','image/png','public',33957,'[]','[]','[]',36,'2019-07-30 10:35:18','2019-07-30 10:35:18'),(39,'App\\Model\\Partner',2,'images','1564457727-rsz_1rsz_mot_logo_png','1564457727-rsz_1rsz_mot_logo_png.png','image/png','public',72228,'[]','[]','[]',37,'2019-07-30 10:35:29','2019-07-30 10:35:29'),(40,'App\\Model\\Partner',1,'images','1564457737-rsz_11rsz_stormy_delivery-01','1564457737-rsz_11rsz_stormy_delivery-01.png','image/png','public',24925,'[]','[]','[]',38,'2019-07-30 10:35:39','2019-07-30 10:35:39'),(42,'App\\Model\\Setting',1,'images','1564457810-rsz_rsz_caa_logo-01','1564457810-rsz_rsz_caa_logo-01.png','image/png','public',33957,'[]','[]','[]',39,'2019-07-30 10:36:52','2019-07-30 10:36:52'),(43,'App\\Model\\Setting',13,'images','1564458498-img-footer-background','1564458498-img-footer-background.jpg','image/jpeg','public',334415,'[]','[]','[]',40,'2019-07-30 10:48:20','2019-07-30 10:48:20'),(44,'App\\Model\\RegisterUser',2,'images','me','me.jpg','image/jpeg','public',191096,'[]','[]','[]',41,'2019-08-09 13:31:14','2019-08-09 13:31:14');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (1,'Cambodia Alumni Association','CEO','2019-07-22 22:34:22','2019-07-22 22:34:22'),(2,'Ny Sominea','Web Application Developer','2019-07-22 22:34:35','2019-07-22 22:34:35');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_07_19_100029_create_banners_table',1),(4,'2019_07_19_100213_create_events_table',1),(5,'2019_07_19_100233_create_members_table',1),(6,'2019_07_19_100339_create_register_users_table',1),(7,'2019_07_19_100924_create_partners_table',1),(8,'2019_07_19_114519_create_provinces_table',1),(9,'2019_07_19_114529_create_galleries_table',1),(10,'2019_07_19_114942_create_principles_table',1),(11,'2019_07_19_123346_create_principle_categories_table',1),(12,'2019_07_19_125355_create_settings_table',1),(13,'2019_07_19_140830_create_permission_tables',1),(14,'2019_07_19_141855_create_media_table',1),(15,'2019_07_25_145540_create_language_lines_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\User',1);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (1,'Cambodia Alumni Association','https://caacambodia.org/','2019-07-22 22:33:09','2019-07-22 22:33:09'),(2,'Tourism',NULL,'2019-07-22 22:33:26','2019-07-22 22:33:26'),(3,'Ny Sominea',NULL,'2019-07-22 22:34:06','2019-07-22 22:34:06');
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'view-project','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(2,'project-modification','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(3,'add-new-project','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(4,'view-project-category','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(5,'project-category-modification','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(6,'add-new-project-category','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(7,'view-user','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(8,'user-modification','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(9,'add-new-user','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(10,'view-role','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(11,'role-modification','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(12,'add-new-role','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(13,'view-news','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(14,'news-modification','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(15,'add-new-news','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(16,'view-career','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(17,'career-modification','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(18,'add-new-career','web','2019-07-22 21:33:10','2019-07-22 21:33:10'),(19,'view-language','web','2019-07-26 10:01:51','2019-07-26 10:01:51'),(20,'language-modification','web','2019-07-26 10:01:51','2019-07-26 10:01:51'),(21,'view-register-user','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(22,'delete-register-user','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(23,'download-register-user','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(24,'view-event','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(25,'event-modification','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(26,'add-new-event','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(27,'view-banner','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(28,'banner-modification','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(29,'add-new-banner','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(30,'view-gallery','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(31,'gallery-modification','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(32,'add-new-gallery','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(33,'view-partner','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(34,'partner-modification','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(35,'add-new-partner','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(36,'view-member','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(37,'member-modification','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(38,'add-new-member','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(39,'view-principle','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(40,'principle-modification','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(41,'add-new-principle','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(42,'view-setting','web','2019-07-26 12:12:52','2019-07-26 12:12:52'),(43,'setting-modification','web','2019-07-26 12:12:52','2019-07-26 12:12:52');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `principle_categories`
--

DROP TABLE IF EXISTS `principle_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `principle_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `principle_categories`
--

LOCK TABLES `principle_categories` WRITE;
/*!40000 ALTER TABLE `principle_categories` DISABLE KEYS */;
INSERT INTO `principle_categories` VALUES (1,'Our Visions',NULL,NULL),(2,'Our Missions',NULL,NULL),(3,'Our Core Values',NULL,NULL);
/*!40000 ALTER TABLE `principle_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `principles`
--

DROP TABLE IF EXISTS `principles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `principles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` tinyint(3) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `principles`
--

LOCK TABLES `principles` WRITE;
/*!40000 ALTER TABLE `principles` DISABLE KEYS */;
INSERT INTO `principles` VALUES (1,'{\"en\":\"The standard Lorem Ipsum passage, used since the 1500s\",\"kh\":null,\"zh\":null}','{\"en\":\"\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\",\"kh\":null,\"zh\":null}',1,'2019-07-22 22:35:01','2019-07-22 22:35:01'),(2,'{\"en\":\"Section 1.10.32 of \\\"de Finibus Bonorum et Malorum\\\", written by Cicero in 45 BC\",\"kh\":null,\"zh\":null}','{\"en\":\"\\\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit\",\"kh\":null,\"zh\":null}',2,'2019-07-22 22:35:42','2019-07-22 22:35:42'),(3,'{\"en\":\"1914 translation by H. Rackham\",\"kh\":null,\"zh\":null}','{\"en\":\"\\\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish.\",\"kh\":null,\"zh\":null}',1,'2019-07-22 22:36:36','2019-07-22 22:36:36'),(4,'{\"en\":\"1914 translation by H. Rackham\",\"kh\":null,\"zh\":null}','{\"en\":\"quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella\",\"kh\":null,\"zh\":null}',2,'2019-07-22 22:37:05','2019-07-22 22:37:05'),(5,'{\"en\":\"The standard Lorem Ipsum passage, used since the 1500s\",\"kh\":null,\"zh\":null}','{\"en\":\"\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\\\"\",\"kh\":null,\"zh\":null}',1,'2019-07-22 23:10:19','2019-07-22 23:10:19'),(6,'{\"en\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit KH\",\"kh\":null,\"zh\":null}','{\"en\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\\\"\",\"kh\":null,\"zh\":null}',3,'2019-07-22 23:11:01','2019-07-22 23:11:01'),(7,'{\"en\":\"1914 translation by H. Rackham\",\"kh\":null,\"zh\":null}','{\"en\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\\\"\",\"kh\":null,\"zh\":null}',3,'2019-07-22 23:11:09','2019-07-22 23:11:09'),(8,'{\"en\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit EN\",\"kh\":null,\"zh\":null}','{\"en\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\\\"\",\"kh\":null,\"zh\":null}',1,'2019-07-22 23:11:29','2019-07-22 23:11:29');
/*!40000 ALTER TABLE `principles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinces` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provinces`
--

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` VALUES (1,'Banteay Meanchey',NULL,NULL,NULL),(2,'Battambang',NULL,NULL,NULL),(3,'Kaeb',NULL,NULL,NULL),(4,'Kampong Cham',NULL,NULL,NULL),(5,'Kampong Chhang',NULL,NULL,NULL),(6,'Kampong Speu',NULL,NULL,NULL),(7,'Kampong Thom',NULL,NULL,NULL),(8,'Kampot',NULL,NULL,NULL),(9,'Kandal',NULL,NULL,NULL),(10,'Koh Kong',NULL,NULL,NULL),(11,'Kratie',NULL,NULL,NULL),(12,'Mondul Kiri',NULL,NULL,NULL),(13,'Oddar Meanchey',NULL,NULL,NULL),(14,'Pailin',NULL,NULL,NULL),(15,'Phnom Penh',NULL,NULL,NULL),(16,'Preah Sihanouk',NULL,NULL,NULL),(17,'Preah Vihear',NULL,NULL,NULL),(18,'Prey Veng',NULL,NULL,NULL),(19,'Pursat',NULL,NULL,NULL),(20,'Ranthanak Kiri',NULL,NULL,NULL),(21,'Siem Reap',NULL,NULL,NULL),(22,'Stung Treng',NULL,NULL,NULL),(23,'Svay Rieng',NULL,NULL,NULL),(24,'Takeo',NULL,NULL,NULL),(25,'Tboung Khmum',NULL,NULL,NULL);
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `register_users`
--

DROP TABLE IF EXISTS `register_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `register_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kh_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `id_number` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `against_humanity` tinyint(1) NOT NULL,
  `address` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `register_users`
--

LOCK TABLES `register_users` WRITE;
/*!40000 ALTER TABLE `register_users` DISABLE KEYS */;
INSERT INTO `register_users` VALUES (1,'Ny Sominea','នី សុមិនា','Male',24,'23649791','Bachelor\'s Degree','[\"Khmer\",\"English\"]','Web Application Developer','(+855) 93 355 045','sominea.ny77@gmail.com',0,'Takhmau, Kandal','https://www.facebook.com/Ny.Sominea','2019-07-26 10:09:45','2019-07-26 10:09:45'),(2,'Ny Sominea','នី សុមិនា','Male',23,'3794822','Bachelor\'s Degree','[\"Khmer\",\"English\"]','Web Application Developer','(+855) 93 355 045','sominea.ny77@gmail.com',0,'Kandal','https://www.facebook.com/Ny.Sominea','2019-08-09 13:31:14','2019-08-09 13:31:14');
/*!40000 ALTER TABLE `register_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Super Administrator','web','2019-07-22 21:33:10','2019-07-22 21:33:10');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'logo','{\"en\":\"\"}',NULL,'2019-07-22 21:42:05'),(2,'site_url','{\"en\":\"https:\\/\\/caacambodia.org\"}',NULL,'2019-07-22 21:42:05'),(3,'site_name','{\"en\":\"Cambodia Alumni Association\"}',NULL,'2019-07-29 17:45:46'),(4,'phone','{\"en\":\"(+855) 93 355 045\"}',NULL,'2019-07-22 21:42:05'),(5,'address','{\"en\":\"Takhmao, Kandal\"}',NULL,'2019-07-22 21:42:05'),(6,'email','{\"en\":\"sominea.ny77@gmail.com\"}',NULL,'2019-07-22 21:42:05'),(7,'copy_right','{\"en\":\"Copy Right @ 2019. All Right Reserve.\"}',NULL,'2019-07-22 21:42:05'),(8,'member_banner','{\"en\":\"\"}',NULL,'2019-07-22 21:43:23'),(9,'about_us_banner','{\"en\":\"\"}',NULL,'2019-07-22 21:43:23'),(10,'register_banner','{\"en\":\"\"}',NULL,'2019-07-22 21:43:23'),(11,'gallery_banner','{\"en\":\"\"}',NULL,'2019-07-22 21:43:23'),(12,'event_banner','{\"en\":\"\"}',NULL,'2019-07-22 21:43:23'),(13,'footer_banner','{\"en\":\"\"}',NULL,'2019-07-22 21:43:23'),(14,'youtube','{\"en\":\"https:\\/\\/www.youtube.com\\/watch?v=ejtrOfdlk1M&list=RDk6ftXI2hlfE&index=10\"}',NULL,'2019-07-22 21:42:14'),(15,'facebook','{\"en\":\"https:\\/\\/www.facebook.com\\/Ny.Sominea\"}',NULL,'2019-07-22 21:42:14'),(16,'linkedin','{\"en\":\"\"}',NULL,'2019-07-22 21:42:14'),(17,'instagram','{\"en\":\"\"}',NULL,'2019-07-22 21:42:14'),(18,'seo_keyword',NULL,NULL,NULL),(19,'seo_description',NULL,NULL,NULL),(20,'our_story_description','{\"en\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. \\r\\nAldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\"}',NULL,'2019-07-22 21:43:57'),(21,'register_description','{\"en\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry.\"}',NULL,'2019-07-22 21:43:57'),(22,'animated_logo','{\"en\":\"\"}','2019-07-30 09:56:40','2019-07-30 09:56:40');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Developer','developer@mkoratec.com',NULL,'$2y$10$OiI6terq4n1fifQmZ1aiT.KAoCDt2k1fTpOoiqWtrLXDmitv.a6/K','g7Awb6Mj2NKmmc4exxTfalpcB7TqZLmVLJ8Tw4dVITf28gGc8tynwq4cu5No','2019-07-22 21:33:10','2019-07-22 21:33:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-12  5:01:25
